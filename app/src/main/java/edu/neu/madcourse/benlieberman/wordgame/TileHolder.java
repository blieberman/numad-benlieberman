package edu.neu.madcourse.benlieberman.wordgame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import edu.neu.madcourse.benlieberman.R;

public class TileHolder extends RelativeLayout {
    ArrayList<Tile> tiles;

    private static final String TAG = "WFHolder";
    private final int HOLDERCOLUMNS = 11;
    private final int HOLDERROWS = 2;

    public int cWidth;    // width of one tile
    public int cHeight;   // height of one tile
    private int selX;       // X index of selection
    private int selY;       // Y index of selection
    public boolean isTileSection;
    public Tile selectedTile;
    private final Rect selRect = new Rect();

    public TileHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.tiles = new ArrayList<>();
        this.setWillNotDraw(false); // Need this or onDraw will never be called
        this.isTileSection = false;
        this.selectedTile = null;
    }

    public ArrayList<Tile> getTiles() {
        return tiles;
    }

    public void setTiles(ArrayList<Tile> tiles) {
        this.tiles = tiles;
    }

    public void addTiles(Tile t) {
        this.tiles.add(t);
    }

    public void removeTiles(Tile t) {
        try {
            this.tiles.remove(t);
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //background style
        Paint background = new Paint();
        background.setColor(Color.WHITE);
        canvas.drawRect(0, 0, getWidth(), getHeight(), background);

        //lets draw the tiles in the set
        cWidth = this.getWidth() / HOLDERCOLUMNS;
        cHeight = this.getHeight() / HOLDERROWS;
        int x = 0;
        int y = 0;

        if (isTileSection) {
            //selected tile styling
            Paint selected = new Paint();
            selected.setColor(getResources().getColor(R.color.puzzle_selected));
            Log.d(TAG, "selRect=" + selRect);
            //canvas.drawRect(0, 0, cWidth, cHeight, selected);
            canvas.drawRect(selRect, selected);
        }

        for(int i = 0; i < tiles.size(); i++) {
            Tile tile = tiles.get(i);
            if(i >= HOLDERCOLUMNS) {
                x = (i - HOLDERCOLUMNS) * cWidth;
                y = cHeight;
            }
            else {
                x = i * cWidth;
                y = 0;
            }
            tile.drawTile3(this.getContext(), canvas, cWidth, cHeight, x, y, SinglePlayerGame.DEFAULT_WORD_ALPHA);
        }
    }

    public void select(int x, int y) {
        invalidate(selRect);
        selX = Math.min(Math.max(x, 0), HOLDERCOLUMNS-1);
        selY = Math.min(Math.max(y, 0), HOLDERROWS-1);
        getRect(selX, selY, selRect);
        getTile(selX, selY);
        Log.d(TAG, "onTouchEvent: x " + selX + ", y " + selY);
        invalidate(selRect);
    }

    private void getRect(int x, int y, Rect rect) {
        rect.set((x * cWidth), (y * cHeight), (x
                * cWidth + cWidth), (y * cHeight + cHeight));
    }

    public void getTile(int x, int y) {
        int i;

        if (y == 0) {
            i = x;
        }
        else {
            i = HOLDERCOLUMNS + x;
        }
        try {
            this.selectedTile = this.tiles.get(i);
        } catch(IndexOutOfBoundsException e) {
            this.selectedTile = null;
            Log.d(TAG, "getTile: index " + i + " is out of bounds");
        }
        Log.d(TAG, "getTile: " + selectedTile);
    }

    public int numTiles() {
        return this.tiles.size();
    }

}