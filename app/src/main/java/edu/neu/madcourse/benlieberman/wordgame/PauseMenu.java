package edu.neu.madcourse.benlieberman.wordgame;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import edu.neu.madcourse.benlieberman.R;

public class PauseMenu extends ActionBarActivity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pause_menu);

        View resumeButton = findViewById(R.id.wg_pause_resume);
        resumeButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wg_pause_resume:
                finish();
                break;
        }
    }
}
