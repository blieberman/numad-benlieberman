package edu.neu.madcourse.benlieberman.wordgame;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import edu.neu.madcourse.benlieberman.R;

public class HighScores extends Activity implements View.OnClickListener {

    private HighScoresAdaptor highScoresAdaptor;
    private ListView listView;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incognito_list);

        // Initialize the subclass of ParseQueryAdapter
        highScoresAdaptor = new HighScoresAdaptor(this);

        // Initialize ListView and set initial view to mainAdapter
        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(highScoresAdaptor);

        // Initilize TextView for title
        title = (TextView) findViewById(R.id.title);
        title.setText("High Scores");
    }

    @Override
    public void onClick(View v) {

    }

    private class HighScoresAdaptor extends ParseQueryAdapter<ParseObject> {

        private static final String PARSE_OBJECT = "Game_Score";

        public HighScoresAdaptor(Context context) {
            // Use the QueryFactory to construct a PQA that will only show
            // Scores only marked as SinglePlayer
            super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {
                public ParseQuery create() {
                    ParseQuery query = new ParseQuery(PARSE_OBJECT);
                    query.whereEqualTo("SinglePlayer", true);
                    query.orderByDescending("score");
                    return query;
                }
            });
        }

        // Customize the layout by overriding getItemView
        @Override
        public View getItemView(ParseObject object, View v, ViewGroup parent) {
            if (v == null) {
                v = View.inflate(getContext(), R.layout.activity_high_scores, null);
            }

            super.getItemView(object, v, parent);

            // Add the score view
            TextView scoreTextView = (TextView) v.findViewById(R.id.score);
            scoreTextView.setText(object.getString("score"));

            // Add the player name view
            TextView titleTextView = (TextView) v.findViewById(R.id.playerName);
            titleTextView.setText(object.getString("playerName"));

            // Add the upload timestamp of the entry
            TextView timestampView = (TextView) v.findViewById(R.id.timestamp);
            timestampView.setText(object.getCreatedAt().toString());
            return v;
        }
    }
}