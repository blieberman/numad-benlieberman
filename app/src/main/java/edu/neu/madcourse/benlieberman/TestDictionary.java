package edu.neu.madcourse.benlieberman;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ArrayAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import edu.neu.madcourse.benlieberman.sudoku.Music;


public class TestDictionary extends ActionBarActivity implements OnClickListener {
    private static final String TAG = "Test Dictionary";
    private static final String FILENAME = "res/raw/wordlist";

    private EditText editWord;

    ArrayList<String> matchedWords=new ArrayList<>();
    ArrayAdapter<String> listAdaptor;
    DatabaseHelper dbHelper;
    ProgressDialog dialog;

    static String DEST_PATH = "/data/data/edu.neu.madcourse.benlieberman/databases/";
    static String DB_NAME = "words.sql";
    static final int DB_VERSION = 1;

    private class DatabaseHelper extends SQLiteOpenHelper {

        SQLiteDatabase mDb = null;

        public DatabaseHelper() {
            super(getApplicationContext(), DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

        public boolean databaseExist(String dbName) {
            File dbFile = getApplicationContext().getDatabasePath(dbName);
            return dbFile.exists();
        }

        public void databaseInit() throws IOException {
            if (!databaseExist(DB_NAME)) {
                mDb = this.getReadableDatabase();
                copyDB();
            }
            else {
                mDb = this.getReadableDatabase();
            }
        }

        public void copyDB() throws IOException {
            Resources res = getResources();
            InputStream is = res.openRawResource(R.raw.words);
            OutputStream os = new FileOutputStream(DEST_PATH + DB_NAME);

            byte [] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            is.close();
            os.flush();
            os.close();
        }

        public boolean containsRow (String s) {
            Cursor c;
            String query = "select 1 from words where content = '"+s+"';";

            c = mDb.rawQuery(query, null);
            if (c.getCount() <= 0) {
                c.close();
                return false;
            }
            else {
                c.close();
                return true;
            }
        }

        @Override
        public void close() {
            if (mDb != null) {
                mDb.close();
            }
        }

    }

    private class DictionaryPrep extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(TestDictionary.this);
            dialog.setMessage("First time initialization...");
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            dbHelper = new DatabaseHelper();
            try {
                dbHelper.databaseInit();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final Void success) {
            if (dialog.isShowing()) {
                dialog.dismiss();

            }
        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_dictionary);

        setTitle(TAG);

        new DictionaryPrep().execute();

        final ListView matchedList = (ListView) findViewById(R.id.listView);

        listAdaptor = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                matchedWords);
        matchedList.setAdapter(listAdaptor);

        // Set up listeners for EditText
        editWord = (EditText) findViewById(R.id.td_editWord);
        editWord.setOnClickListener(this);
        editWord.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() >= 3) {
                    String userInput = s.toString();
                    userInput = userInput.toLowerCase();
                    if (dbHelper.containsRow(userInput) && !matchedWords.contains(userInput)) {
                        matchedWords.add(userInput);
                        listAdaptor.notifyDataSetChanged();
                        Music.play(getApplicationContext(), R.raw.two_tone_nav);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        // Set up click listeners for all the buttons
        View clearButton = findViewById(R.id.td_button_clear);
        clearButton.setOnClickListener(this);
        View acknowledgementsButton = findViewById(R.id.td_button_acknowledgements);
        acknowledgementsButton.setOnClickListener(this);
        View returnButton = findViewById(R.id.td_button_return);
        returnButton.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            dbHelper.close();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.td_button_clear:
                clearText();
                break;
            case R.id.td_button_acknowledgements:
                openAcknowledgements();
                break;
            case R.id.td_button_return:
                finish();
                break;
        }
    }

    private void openAcknowledgements() {
        Intent intent = new Intent(this, Acknowledgements.class);
        startActivity(intent);
    }

    /**
     * clearText() : clears the string of an EditText
     */
    void clearText() {
        editWord.getText().clear();
        if (!matchedWords.isEmpty()) {
            matchedWords.clear();
            listAdaptor.notifyDataSetChanged();
        }
    }
}