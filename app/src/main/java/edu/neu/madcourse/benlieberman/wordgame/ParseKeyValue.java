package edu.neu.madcourse.benlieberman.wordgame;


public class ParseKeyValue {

    private String key;
    private Object value;

    public ParseKeyValue(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    //public String getValue() {
    //    return value;
    //}

    public String getValue() {
        return (this.value == null) ? "null" : value.toString();
    }

    public void setValue(Object value) {
        this.value = value;
    }
}