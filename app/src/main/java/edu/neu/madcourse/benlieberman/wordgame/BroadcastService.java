package edu.neu.madcourse.benlieberman.wordgame;

import android.content.Intent;
import android.content.Context;

public class BroadcastService {
    private static final String ACTION_SEND = "WG.SEND_TEST";

    public static void startSendTest(Context context) {
        Intent intent = new Intent(context, BroadcastService.class);
        intent.setAction(ACTION_SEND);
        context.startService(intent);
    }
}
