package edu.neu.madcourse.benlieberman.wordgame;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import edu.neu.madcourse.benlieberman.R;

public class GCMIntentService extends IntentService {
    public static final int INV_NOTIFICATION_ID = 1;
    public static final int MOV_NOTIFICATION_ID = 2;
    NotificationCompat.Builder builder;
    static final String TAG = "GCM_Communication";

    public GCMIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent incoming) {
        NotificationManager nManager = (NotificationManager) this.
                getSystemService(Context.NOTIFICATION_SERVICE);

        Log.d(TAG, "HI");

        Bundle extras = incoming.getExtras();
        Log.d(TAG, extras.toString());

        boolean isObservation = Boolean.parseBoolean(extras.getString(CommunicationConstants.OBSERVATION_MODE));
        String GID = extras.getString("GAMEID");

        if (incoming.getStringExtra("KIND") == null) {
            GCMBroadcastReceiver.completeWakefulIntent(incoming);
            return;
        }

        switch (incoming.getStringExtra("KIND")) {
            case "invite":
                Intent i = new Intent(this, TwoPlayerGame.class);
                i.putExtra(CommunicationConstants.OBSERVATION_MODE, isObservation);
                i.putExtra("PID", extras.getString("PID"));
                i.putExtra("IID", extras.getString("IID"));
                i.putExtra("GAMEID", extras.getString("GAMEID"));

                PendingIntent intent = PendingIntent.getActivity(this, 0,
                        i,
                        PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("WordFade Invitation")
                        .setStyle(
                                new NotificationCompat.BigTextStyle()
                                        .bigText("WordFade Invitation"))
                        .setContentText("Someone wants to play WordFade...").setTicker("WordFade Invite")
                        .setAutoCancel(true);
                mBuilder.setContentIntent(intent);
                nManager.notify(INV_NOTIFICATION_ID, mBuilder.build());

                GCMBroadcastReceiver.completeWakefulIntent(incoming);
                return;

            case "move":
                Intent notifyForMoveCheck;
                if (isObservation) {
                    Log.d(TAG, "OBSERVATION -- MOVED DETECTED + " + isObservation);

                    notifyForMoveCheck = new Intent(ObservationGame.RECEIVE_TEST);
                    notifyForMoveCheck.putExtra("GAMEID", GID);
                    notifyForMoveCheck.putExtra(CommunicationConstants.OBSERVATION_MODE, isObservation);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(notifyForMoveCheck);

                    GCMBroadcastReceiver.completeWakefulIntent(incoming);
                    return;
                }
                else {
                    Log.d(TAG, "TWOPLAYERGAME -- MOVED DETECTED + " + isObservation);

                    Intent intent1 = new Intent(this, TwoPlayerGame.class);
                    intent1.putExtra(CommunicationConstants.OBSERVATION_MODE, isObservation);
                    intent1.putExtra("GAMEID", extras.getString("GAMEID"));

                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                            intent1,
                            PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

                    NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(
                            this)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle("WordFade Move Alert")
                            .setStyle(
                                    new NotificationCompat.BigTextStyle()
                                            .bigText("New Moves"))
                            .setContentText("New Moves").setTicker("New Moves")
                            .setAutoCancel(true);
                    nBuilder.setContentIntent(pendingIntent);

                    nManager.notify(MOV_NOTIFICATION_ID, nBuilder.build());

                    // local notify for sync move
                    notifyForMoveCheck = new Intent(TwoPlayerGame.RECEIVE_MAIN);
                    notifyForMoveCheck.putExtra("GAMEID", GID);
                    notifyForMoveCheck.putExtra(CommunicationConstants.OBSERVATION_MODE, isObservation);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(notifyForMoveCheck);

                    GCMBroadcastReceiver.completeWakefulIntent(incoming);
                    return;
                }

        }
        // Release the wake lock provided by the WakefulBroadcastReceiver
        GCMBroadcastReceiver.completeWakefulIntent(incoming);
    }
}