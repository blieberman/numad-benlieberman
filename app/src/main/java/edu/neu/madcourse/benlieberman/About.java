package edu.neu.madcourse.benlieberman;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.content.Context;
import android.widget.TextView;

public class About extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(edu.neu.madcourse.benlieberman.R.layout.activity_about);

        // Let's get the device id
        String uniqueId = null; // initialize the string to hold the id
        TelephonyManager telephonyManager; // declare a telephony manager
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        uniqueId = telephonyManager.getDeviceId();

        // Let's push the string to layout
        TextView textView = (TextView) findViewById(edu.neu.madcourse.benlieberman.R.id.device_id);
        textView.setText(uniqueId);

    }

}