package edu.neu.madcourse.benlieberman.wordgame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import edu.neu.madcourse.benlieberman.R;

/*
Ben Lieberman
Northeastern University
Main activity for Banannagrams like word game
 */

public class WordGame extends Activity implements OnClickListener {
    private static final String TAG = "WordFade";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_game);

        // High Scores
        View highScoresButton = findViewById(R.id.wg_main_button_highscores);
        highScoresButton.setOnClickListener(this);

        // Game List
        View gameListButton = findViewById(R.id.wg_main_button_gamelist);
        gameListButton.setOnClickListener(this);

        // New two player game
        View newSPButton = findViewById(R.id.wg_main_button_singleplayer);
        newSPButton.setOnClickListener(this);

        // New two player game
        View newTPButton = findViewById(R.id.wg_main_button_twoplayer);
        newTPButton.setOnClickListener(this);

        // Quit game
        View quitButton = findViewById(R.id.wg_main_button_quit);
        quitButton.setOnClickListener(this);

        CommunicationTasks.RegisterDevice(this, this.getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wg_main_button_singleplayer:
                newSinglePlayerGame();
                break;
            case R.id.wg_main_button_twoplayer:
                newTwoPlayerGame();
                break;
            case R.id.wg_main_button_highscores:
                Intent intent = new Intent(this, HighScores.class);
                startActivity(intent);
                break;
            case R.id.wg_main_button_quit:
                finish();
                break;
            case R.id.wg_main_button_acknowledgements:
                break;
            case R.id.wg_main_button_gamelist:
                openGameList();
                break;
        }
    }

    /** Calls starts a single player game with a blank slate */
    private void newSinglePlayerGame() {
        Intent intent = new Intent(this, SinglePlayerGame.class);
        startActivity(intent);
    }

    /** Starts the two player game activity */
    private void newTwoPlayerGame() {
        Intent intent = new Intent(this, RegisteredUsers.class);
        intent.putExtra(CommunicationConstants.OBSERVATION_MODE, false);
        startActivity(intent);
    }

    /** Starts the game list activity */
    private void openGameList() {
        Intent intent = new Intent(this, GameList.class);
        intent.putExtra(CommunicationConstants.OBSERVATION_MODE, false);
        startActivity(intent);
    }

}