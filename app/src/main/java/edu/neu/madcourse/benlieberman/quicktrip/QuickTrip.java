package edu.neu.madcourse.benlieberman.quicktrip;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import edu.neu.madcourse.benlieberman.R;

public class QuickTrip extends ActionBarActivity implements OnClickListener {

    private static final String TAG = "QuickTrip";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_trip);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setIcon(R.drawable.ic_quicktrip);

        // Logo imageView
        ImageView logo = (ImageView) findViewById(R.id.imageView_qt_logo);
        Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);
        fadeIn.setDuration(2000);
        logo.startAnimation(fadeIn);

        // Show routes button
        View showRoutesButton = findViewById(R.id.qt_mm_btn_sr);
        showRoutesButton.setOnClickListener(this);

        // Plan ahead button
        View planAheadButton = findViewById(R.id.qt_mm_btn_pa);
        planAheadButton.setOnClickListener(this);

        NetworkFunctions.cancelNotifications(this);

        if (!NetworkFunctions.isConnectedToInternet(this)) {
            AlertDialog connectionAlert = NetworkFunctions
                    .buildDialog(this, "No connection detected, exiting...", "Network Error");
            connectionAlert.show();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.qt_mm_btn_sr:
                Intent srIntent = new Intent(this, ShowRoutes.class);
                startActivity(srIntent);
                break;
            case R.id.qt_mm_btn_pa:
                Intent paIntent = new Intent(this, PlanAhead.class);
                startActivity(paIntent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quick_trip, menu);
        return true;
    }
}