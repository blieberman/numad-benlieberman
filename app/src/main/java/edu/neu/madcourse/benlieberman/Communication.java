package edu.neu.madcourse.benlieberman;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.parse.ParseObject;

import java.util.Random;

import edu.neu.madcourse.benlieberman.wordgame.CommunicationConstants;
import edu.neu.madcourse.benlieberman.wordgame.ObservationGame;
import edu.neu.madcourse.benlieberman.wordgame.RegisteredUsers;
import edu.neu.madcourse.benlieberman.wordgame.HighScores;
import edu.neu.madcourse.benlieberman.wordgame.CommunicationTest;

public class Communication extends Activity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication);

        findViewById(R.id.communication_button_ce).setOnClickListener(this);
        findViewById(R.id.communication_button_ghs).setOnClickListener(this);
        findViewById(R.id.communication_button_shs).setOnClickListener(this);
        findViewById(R.id.communication_button_fp).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.communication_button_fp:
                openFindPlayers();
                break;
            case R.id.communication_button_shs:
                setRandomHighScore();
                break;
            case R.id.communication_button_ghs:
                openGetHighScores();
                break;
            case R.id.communication_button_ce:
                openCommunicationExample();
                break;
        }
    }

    /** Starts the find registered users activity */
    private void openFindPlayers() {
        Intent intent = new Intent(this, RegisteredUsers.class);
        intent.putExtra(CommunicationConstants.OBSERVATION_MODE, true);
        startActivity(intent);
    }

    /** Starts the high scores activity */
    private void openGetHighScores() {
        Intent intent = new Intent(this, HighScores.class);
        startActivity(intent);
    }

    /** Starts the communication example activity */
    private void openCommunicationExample() {
        Intent intent = new Intent(this, CommunicationTest.class);
        startActivity(intent);
    }

    private void setRandomHighScore() {
        Random rand = new Random();
        int randomScore = rand.nextInt((100 - 0) + 1) + 0;

        ParseObject gameScore = new ParseObject("Game_Score");
        gameScore.put("score", randomScore+"");
        gameScore.put("playerName", "Test Score");
        gameScore.put("SinglePlayer", true);
        gameScore.saveInBackground();

        Toast.makeText(getApplicationContext(), "Sending score of " + randomScore,
                Toast.LENGTH_LONG).show();
    }

}
