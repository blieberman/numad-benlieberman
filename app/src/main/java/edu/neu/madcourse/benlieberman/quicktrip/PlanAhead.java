package edu.neu.madcourse.benlieberman.quicktrip;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import edu.neu.madcourse.benlieberman.R;

/* Concepts taken from
*  http://javapapers.com/android/android-alarm-clock-tutorial/
*/

public class PlanAhead extends ActionBarActivity implements OnClickListener {

    private static final String TAG = "PlanAhead";

    private Spinner routeSpinner;
    private TimePicker timePicker;
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;

    private SharedPreferences sp;
    View setAlarmToggle;

    private final String[] routes = new String[] {
            "177 Huntington (from WVH)",
            "WVH (from 177 Huntington)"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_ahead);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setIcon(R.drawable.ic_quicktrip);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Plan Ahead");
        actionBar.setSubtitle("Select a destination and time");

        // Initialize spinner
        routeSpinner = (Spinner) findViewById(R.id.routeSpinner);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, routes);

        routeSpinner.setAdapter(spinnerAdapter);

        // Initialize time picker
        timePicker = (TimePicker) findViewById(R.id.timePicker);

        // Set alarm button
        View setAlarmToggle = findViewById(R.id.qt_setalarm);
        setAlarmToggle.setOnClickListener(this);

        // Set alarm manager
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // Check if alarm has been set -- from SharedPreferences
        sp = getSharedPreferences(TAG, MODE_PRIVATE);
        Boolean isAlarmSet = sp.getBoolean("alarm_set_flag", false);

        if(!isAlarmSet) {
            ((ToggleButton) setAlarmToggle).setChecked(false);
        }
        else {
            ((ToggleButton) setAlarmToggle).setChecked(true);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.qt_setalarm:
                if ( ((ToggleButton) v).isChecked()) {
                    setAlarmOn();
                    Log.d(TAG, "Alarm on");
                    break;
                }
                else {
                    setAlarmOff();
                    Log.d(TAG, "Alarm off");
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quick_trip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setAlarmOn() {
        Calendar calendar = Calendar.getInstance();
        Calendar calendarNow = Calendar.getInstance();

        int tpHour = timePicker.getCurrentHour();
        int tpMinute = timePicker.getCurrentMinute();
        String routeSelection = routeSpinner.getSelectedItem().toString();

        // get hour from picker
        calendar.set(Calendar.HOUR_OF_DAY, tpHour);
        // get minute from picker
        calendar.set(Calendar.MINUTE, tpMinute);
        calendar.add(Calendar.MINUTE, -15);

        Date currentTime;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss z");

        currentTime = calendarNow.getTime();

        Log.d(TAG, "setAlarmOn-- setTime=" + dateFormatter.format(calendar.getTime()) + " "
        + "currTime=" + dateFormatter.format(currentTime));

        if (calendar.getTime().before(currentTime)) {
            String toastMessage = "INVALID TIME";
            Toast toast = Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return;
        }

        Log.d(TAG, "" + dateFormatter.format(calendar.getTime()));

        Intent i = new Intent(PlanAhead.this, PlanAheadReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(PlanAhead.this, 0, i, 0);
        // set alarm for specific time
        alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);

        // set in saved prefs
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("alarm_set_flag", true); // value to store
        editor.apply();

        String toastMessage = "Planning service set for " + tpHour + ":" + tpMinute
                + " arrival time to " + routeSelection;
        Toast toast = Toast.makeText(this, toastMessage, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void setAlarmOff() {
        alarmManager.cancel(pendingIntent);

        String toastMessage = "Cancelling planning service...";

        // set in saved prefs
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("alarm_set_flag", false); // value to store
        editor.apply();

        Toast toast = Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

}