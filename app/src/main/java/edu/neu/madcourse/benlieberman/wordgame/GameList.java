package edu.neu.madcourse.benlieberman.wordgame;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.text.Format;
import java.text.SimpleDateFormat;

import edu.neu.madcourse.benlieberman.R;

public class GameList extends Activity implements View.OnClickListener {

    private GameListAdaptor gameListAdaptor;
    private ListView listView;
    private TextView title;
    public static String gameID;
    private static String TAG = "GAME_LIST";

    String userName;
    Boolean isObservation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incognito_list);

        // Initialize the subclass of ParseQueryAdapter
        gameListAdaptor = new GameListAdaptor(this);

        isObservation = getIntent().getExtras().getBoolean(CommunicationConstants.OBSERVATION_MODE);
        userName = CommunicationTasks.GetAccountName(GameList.this);

        // Initialize ListView and set initial view to mainAdapter
        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(gameListAdaptor);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseObject parseObject = (ParseObject) listView.getItemAtPosition(position);
                gameID = parseObject.getString("gameID");

                Log.d(TAG, "onItemClick: game clicked is " + gameID);

                Intent intent = new Intent(getApplicationContext(), TwoPlayerGame.class);
                intent.putExtra(CommunicationConstants.OBSERVATION_MODE, false);
                intent.putExtra("GAMEID", gameID);
                startActivity(intent);
            }
        });

        // Initilize TextView for title
        title = (TextView) findViewById(R.id.title);
        title.setText("Games I need to make move...");
    }

    @Override
    public void onClick(View v) {

    }

    private class GameListAdaptor extends ParseQueryAdapter<ParseObject> {

        private static final String PARSE_OBJECT = "Games";

        public GameListAdaptor(Context context) {
            // Use the QueryFactory to construct a PQA that will only show
            // Scores only marked as SinglePlayer
            super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {
                public ParseQuery create() {
                    ParseQuery query = new ParseQuery(PARSE_OBJECT);
                    query.whereEqualTo("turnUser", userName);
                    query.orderByDescending("updatedAt");
                    return query;
                }
            });
        }

        // Customize the layout by overriding getItemView
        @Override
        public View getItemView(ParseObject object, View v, ViewGroup parent) {
            if (v == null) {
                v = View.inflate(getContext(), R.layout.activity_game_list, null);
            }

            super.getItemView(object, v, parent);

            // Add the opponent name view
            TextView oppPlayerNameTextView = (TextView) v.findViewById(R.id.oppPlayerName);
            oppPlayerNameTextView.setText(object.getString("opponentName"));

            // Add the last modified date view
            TextView timestampView = (TextView) v.findViewById(R.id.gameLastModified);
            timestampView.setText(object.getCreatedAt().toString());

            return v;
        }
    }
}