package edu.neu.madcourse.benlieberman.wordgame;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TwoPlayerGame extends AbstractGame {

    String PID;
    boolean isObservation;
    boolean isYourTurn = false;
    static final String TAG = "TWO_PLAYER_GAME";
    public static final String RECEIVE_MAIN = "WG.RECEIVE_MAIN";
    String userName;
    static String otherPlayerName;
    static String gameID;
    String boardString = null;
    static String receiveString;
    static private Map<String, ArrayList<int[]>> receivedWords = new HashMap<>();
    static AlertDialog turnAlert;
    static int wordCount = 0;

    private TwoPlayerReceiver receiver;
    LocalBroadcastManager manager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        gameID = getIntent().getStringExtra("GAMEID");

        isObservation = getIntent().getExtras().getBoolean(CommunicationConstants.OBSERVATION_MODE);
        userName = CommunicationTasks.GetAccountName(this);

        manager = LocalBroadcastManager.getInstance(this);
        receiver = new TwoPlayerReceiver(new Handler());
        manager.registerReceiver(receiver, new IntentFilter(RECEIVE_MAIN));


        setSendToPlayer();
        getTurnInfo();

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

       new RetrieveMoveData().execute();
    }

    @Override
    public void gameOver() {
        // delete the game data entry from Parse.com
        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");
        query.whereEqualTo("gameID", gameID);
        query.whereEqualTo("isSpectatorMode", isObservation);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("delGameFromCloud", "The getFirst request failed.");
                } else {
                    Log.d("delGameFrom", "Retrieved the object.");
                    object.deleteInBackground();
                }
            }
        });

        super.gameOver();
    }

    @Override
    protected void setWords() {
        super.setWords();

        if (isNewWord() && !isObservation) {
            String s = mapToJson(board.wordsCreated);

            boardString = s;
            // send data to cloud and then send notif
            new SendBoardToCloud().execute(boardString);

            Log.d(TAG, gameID + ": Sending move...");
            Log.d(TAG, "DumpJSON: " + boardString);

            wordCount = board.wordsCreated.size();
            isYourTurn = false;
            saveTurnPlayer();
            setTurnLockout();
        }

        if (isObservation) {
            String s = mapToJson(board.wordsCreated);

            boardString = s;
            // send data to cloud and then send notif
            new SendBoardToCloud().execute(boardString);
        }

        pauseWordTimers();
    }

    protected boolean isNewWord() {
        Log.d(TAG, "isNewWord: " + "wordsCreated=" + board.wordsCreated.size()
                + " ; " + "wordcount=" + wordCount);

        return board.wordsCreated.size() > wordCount;
    }

    protected static void dumpOpponentTurn() {
        wordCount = 0;
        for (Map.Entry<String, ArrayList<int[]>> entry : receivedWords.entrySet()) {
            final String k = entry.getKey();
            final ArrayList<int[]> v = entry.getValue();
            wordCount += 1;

            int i = 0;

            String cords = "";
            for (int[] posn : v) {
                int x = posn[0];
                int y = posn[1];

                char letter  = k.charAt(i);
                int point = letterToPoint.get(letter);

                Tile tile = new Tile(letter,point);
                tile.isMovable = false;

                board.setTile(x, y, tile);

                cords += x;
                cords += ",";
                cords += y;
                cords += " ";

                i++;
            }
            Log.d(TAG, "dumpOpponentTurn: WORD=" + k + " | CORDS=" + cords);
        }
    }

    protected String mapToJson(Map map) {
        Gson gson = new Gson();
        String json = gson.toJson(map);

        return json;
    }

    static Map<String, ArrayList<int[]>> jsonToMap(String json) {
        Gson gson = new Gson();
        Type stringArrayListMap = new TypeToken<Map<String, ArrayList <int []>>>(){}.getType();
        return gson.fromJson(json, stringArrayListMap);
    }

    void sendNotification() {
        if(isObservation) {
            CommunicationTasks.sendMoves(ObservationGame.RECEIVE_TEST, PID, this, isObservation, gameID);
        }
        else {
            CommunicationTasks.sendMoves(TwoPlayerGame.RECEIVE_MAIN, PID, this, false, gameID);
        }
    }

    private class SendBoardToCloud extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(TwoPlayerGame.this);
        }

        @Override
        protected Void doInBackground(final String... params) {

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");
            query.whereEqualTo("gameID", gameID);

            // Retrieve the object by id
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                public void done(ParseObject currGame, ParseException e) {
                    if (e == null) {
                        // Now let's update it with some new data...
                        currGame.put("data", params[0]);
                        try {
                            currGame.save();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void success) {
            // send notification to opponent
            sendNotification();
        }
    }

    public class TwoPlayerReceiver extends BroadcastReceiver {

        private final Handler handler; // Handler used to execute code on the UI thread

        public TwoPlayerReceiver(Handler handler) {
            this.handler = handler;
        }

        @Override
        public void onReceive(final Context context, Intent intent) {

            Log.d(TAG, "onReceive: move detected...");

            new RetrieveMoveData().execute();

            //moveTV.invalidate();

            // Post the UI updating code to our Handler
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Opponent has moved tiles.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    protected void setSendToPlayer() {
        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");
        query.whereEqualTo("gameID", gameID);
        query.whereEqualTo("isSpectatorMode", isObservation);

        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                String uName;
                if (object == null) {
                    Log.d(TAG, "The Games request does not exist | failed.");
                } else {
                    Log.d(TAG, "Retrieved the Games object");

                    if (userName.contentEquals(object.getString("playerName"))) {
                        uName = object.getString("opponentName");
                        Log.d(TAG, "MYNAME="+userName + " | " + "sending to " + uName);

                        retreiveRegID(uName);
                        setOtherPlayerName(uName);
                    }
                    else {
                        uName = object.getString("playerName");
                        Log.d(TAG, "MYNAME="+userName + " | " + "sending to " + uName);

                        retreiveRegID(uName);
                        setOtherPlayerName(uName);
                    }
                }
            }
        });
    }

    private void retreiveRegID(String name) {
        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Registered_Users");
        query.whereEqualTo("playerName", name);

        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("getRegIdFromCloud", "The getFirst request failed.");
                } else {
                    Log.d("getRegIdFromCloud", "Retrieved the object.");
                    PID = object.getString("regId");
                }
            }
        });
    }

    private void getTurnInfo() {
        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");
        query.whereEqualTo("gameID", gameID);
        query.whereEqualTo("isSpectatorMode", isObservation);

        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                String uName;
                if (object == null) {
                    Log.d(TAG, "The Games request does not exist | failed.");
                } else {
                    Log.d(TAG, "Retrieved the Games object");

                    uName = object.getString("turnUser");

                    if (uName.contentEquals(userName)) {
                        Log.d(TAG, "It's your turn!");
                        isYourTurn = true;
                        setTurnUnlock();
                    }
                    else {
                        Log.d(TAG, "It's not your turn!");
                        isYourTurn = false;
                        setTurnLockout();
                    }
                }
            }
        });
    }

    private void setTurnLockout() {
        if (!isYourTurn) {
            Log.d(TAG, "locking board");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Not your turn...")
                    .setCancelable(false);
            turnAlert = builder.create();
            turnAlert.show();
        }
    }

    private void setTurnUnlock() {
        if (isYourTurn && turnAlert != null) {
            Log.d(TAG, "unlocking board");
            turnAlert.dismiss();
        }
    }

    private class RetrieveMoveData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");
            query.whereEqualTo("gameID", gameID);
            ParseObject currGame;

            // Retrieve the object by id
            try {
                currGame = query.getFirst();
                receiveString = currGame.getString("data");
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "RetrieveMoveData: Valid query, let's grab the data");

            Log.d(TAG, "RECEIVESTRING= " + receiveString);
            return null;
        }

        @Override
        protected void onPostExecute(Void success) {
            receivedWords = jsonToMap(receiveString);

            Log.d(TAG, "RetriveMoceData: onPostExecute...");
            if(receivedWords != null) {
                dumpOpponentTurn();
            }
            isYourTurn = true;
            setTurnUnlock();
            board.invalidate();
        }
    }

    protected static void setOtherPlayerName(String name) {
        otherPlayerName = name;
    }

    protected void saveTurnPlayer() {
        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");
        query.whereEqualTo("gameID", gameID);
        query.whereEqualTo("isSpectatorMode", isObservation);

        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d(TAG, "The Games request does not exist | failed.");
                } else {
                    Log.d(TAG, "Retrieved the Games object");
                    object.put("turnUser", otherPlayerName);
                    Log.d(TAG, "saveTurnPlayer: setting and push to " + otherPlayerName);

                }
            }
        });
    }

}