package edu.neu.madcourse.benlieberman.sudoku;

import android.app.Activity;
import android.os.Bundle;

import edu.neu.madcourse.benlieberman.R;

public class About extends Activity {
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.about);
   }
}