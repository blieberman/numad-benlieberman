package edu.neu.madcourse.benlieberman.quicktrip;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.Header;
import org.json.*;
import com.loopj.android.http.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import edu.neu.madcourse.benlieberman.R;

public class TrickiestPart extends Activity implements OnClickListener {

    private static final String TAG = "TrickiestPart";
    private static Map<String,Long> tripsWithTimes = new TreeMap<>();
    private static ArrayList<String> quickestTrips =  new ArrayList<>();

    ArrayAdapter<String> quickTripAdaptor;

    /* TRAVEL CONSTANTS */
    //total time between pru and neu green line stops (both ways)
    private static final long MIN_SRIDE_BETWEEN_GREENLINE = 3;
    // time to walk to neu s stop from wvh
    private static final long MIN_WALK_TO_S_NEU = 5;
    // time to walk to pru s stop from 177
    private static final long MIN_WALK_TO_S_PRU = 2;
    // time to walk between wvh and pru (both ways)
    private static final long MIN_WALK_BETWEEN_LOCS = 14;

    private static String subwayTimeText = null;

    TextView routeInfo;
    ListView routeList;

    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trickiest_part);

        // WVH -> 177 Huntington Button
        View wvhToHButton = findViewById(R.id.final_bt_wvhtoh);
        wvhToHButton.setOnClickListener(this);

        // 177 Huntington -> WVH
        View hToWvhButton = findViewById(R.id.final_bt_htowvh);
        hToWvhButton.setOnClickListener(this);

        // TextView
        routeInfo = (TextView) findViewById(R.id.final_txt_results);
        // ListView
        routeList = (ListView) findViewById(R.id.final_list_quickroutes);

        // Adaptor for ListView
        quickTripAdaptor = new ArrayAdapter<>(this,
                android.R.layout.simple_expandable_list_item_1,
                quickestTrips);

        routeList.setAdapter(quickTripAdaptor);
        routeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String route = (String) routeList.getItemAtPosition(position);
                Log.d(TAG, "onItemClick: route clicked is " + route);

                if (!route.isEmpty()) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(TrickiestPart.this);
                    builder.setTitle("Route Information");

                    if (route.contains("E-Line")) {
                        builder.setMessage(subwayTimeText);
                    }

                    builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (TrickiestPart.this.dialog != null)
                                TrickiestPart.this.dialog.dismiss();
                        }
                    });

                    dialog = builder.create();
                    dialog.show();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        quickTripAdaptor.clear();

        switch (v.getId()) {
            case R.id.final_bt_wvhtoh:
                wvhToHQuery();
                break;
            case R.id.final_bt_htowvh:
                hToWvhQuery();
                break;
        }
    }

    private void wvhToHQuery() {
        try {
            RestClientMBTAUsage.getArrivalTimes("s_neu");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String wvhTransKey = RestClientMBTAUsage.calcFirstOption(
                tripsWithTimes, MIN_WALK_TO_S_NEU);
        long wvhShortest = RestClientMBTAUsage.calcRouteTime(wvhTransKey, tripsWithTimes);

        populateList(wvhShortest + " minutes via MBTA E-Line outbound from NEU");
        populateList(MIN_WALK_BETWEEN_LOCS + " minutes via walking");

        if (wvhShortest< MIN_WALK_BETWEEN_LOCS) {
        }
        else {
        }
    }

    private void hToWvhQuery() {
        try {
            RestClientMBTAUsage.getArrivalTimes("s_pru");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String huntTransKey = RestClientMBTAUsage.calcFirstOption(
                tripsWithTimes, MIN_WALK_TO_S_PRU);

        long huntShortest = RestClientMBTAUsage.calcRouteTime(huntTransKey, tripsWithTimes);

        populateList(huntShortest + " minutes via MBTA E-Line inbound from PRU");
        populateList(MIN_WALK_BETWEEN_LOCS + " minutes via walking");

        if (huntShortest< MIN_WALK_BETWEEN_LOCS) {
        }
        else {
        }
    }

    private void populateList(String route) {
        quickTripAdaptor.add(route);
    }

    private static class RestClientMBTAUsage {
        private static final String API_KEY = "fBkegU3LMES0GwQlYmLKDg";

        private static final String WVH_LAT = "42.338822";
        private static final String WVH_LON = "-71.092312";

        private static final String STOPS_BY_LOCATION = "stopsbylocation?api_key=" + API_KEY +
                "&lat=" + WVH_LAT + "&lon=" + WVH_LON + "&format=json";

        // neu subway query
        private static final String S_PREDICTIONS_BY_NEU = "schedulebystop?api_key=" + API_KEY +
                "&stop=" + "place-nuniv" + "&route=" + "Green-E" + "&direction=" + "1" + "&format=json";

        // prudential subway query
        private static final String S_PREDICTIONS_BY_PRU = "schedulebystop?api_key=" + API_KEY +
                "&stop=" + "place-prmnl" + "&route=" + "Green-E" + "&direction=" + "0" + "&format=json";

        // given an epoch, returns the difference between the current date in minutes
        private static long epochDiffToTime(long until) {
            SimpleDateFormat formatDate = new SimpleDateFormat("dd hh:mm a");

            //format until epoch to date
            Date untilD = new Date(until * 1000);
            String uDate = formatDate.format(untilD);

            //get system date
            Date currD = new Date();
            String cDate = formatDate.format(currD);

            long diffInMillis = getDateDiff(currD, untilD, TimeUnit.MINUTES);
            // difference in minutes
            return diffInMillis;
        }

        private static String calcFirstOption(Map<String,Long> map, long timeAway) {
            for (Map.Entry<String, Long> entry : map.entrySet()) {
                String key = entry.getKey();
                long arrivalTime = entry.getValue();
                long etaMins = epochDiffToTime(arrivalTime);

                if (etaMins >= timeAway) {
                    Log.d(TAG, "calcFirstOption-- trans:" + key + "| eta:" +etaMins + " min");
                    return key;
                }
            }
            return "";
        }

        private static long calcRouteTime(String key, Map<String,Long> map) {
            if (!key.isEmpty()) {
                long arrivalTime = map.get(key);
                long etaMins = epochDiffToTime(arrivalTime);

                if (key.contains("Heath Street to Lechmere - Outbound")) {
                    // is from WVH to 177 via Subway @ NEU
                    long totalTime = MIN_WALK_TO_S_NEU + (etaMins - MIN_WALK_TO_S_NEU)
                            + MIN_SRIDE_BETWEEN_GREENLINE + MIN_WALK_TO_S_PRU;

                    subwayTimeText =
                            "- Walk " + MIN_WALK_TO_S_NEU + " min " + "to NEU Green Line (Outbound) stop\n" +
                                    "- Wait " + (etaMins - MIN_WALK_TO_S_NEU) + " min\n" +
                                    "- Ride for " + MIN_SRIDE_BETWEEN_GREENLINE + " min\n" +
                                    "- Walk " + MIN_WALK_TO_S_PRU + " min to 177 Huntington";

                    Log.d(TAG, "calcRouteTime-- ROUTE TOTAL TIME:" + totalTime);
                    return totalTime;
                }
                if (key.contains("Lechmere - Inbound to Heath Street")) {
                    // is from 177 to WVH via Subway @ PRU
                    long totalTime = MIN_WALK_TO_S_PRU + (etaMins - MIN_WALK_TO_S_PRU)
                            + MIN_SRIDE_BETWEEN_GREENLINE + MIN_WALK_TO_S_NEU;

                    subwayTimeText =
                            "- Walk " + MIN_WALK_TO_S_NEU + " min " + " to PRU Green Line (Inbound) stop\n" +
                                    "- Wait " + (etaMins - MIN_WALK_TO_S_PRU) + " min\n" +
                                    "- Ride for " + MIN_SRIDE_BETWEEN_GREENLINE + " min\n" +
                                    "- Walk " + MIN_WALK_TO_S_NEU + " min to WVH";

                    Log.d(TAG, "calcRouteTime-- ROUTE TOTAL TIME:" + totalTime);
                    return totalTime;
                }
            }
            return 0;
        }

        private static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
            long diffInMillis = date2.getTime() - date1.getTime();
            return timeUnit.convert(diffInMillis, TimeUnit.MILLISECONDS);
        }

        private static void getCloseStop() throws JSONException {
            MBTARestClient.get(STOPS_BY_LOCATION, null,
                    new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers,
                                      JSONObject jsonObject) {
                    Log.d(TAG, "OnSuccess");

                    // Pull out the first event on the public timeline
                    JSONArray stops;

                    try {
                        stops = jsonObject.getJSONArray("stop");
                        for (int i = 0 ; i <= 0; i++) {
                            JSONObject stop = stops.getJSONObject(i);

                            String stopId;
                            String stopName;
                            String stopDistance;

                            stopId = stop.getString("stop_id");
                            stopName = stop.getString("stop_name");
                            stopDistance = stop.getString("distance");

                            Log.d(TAG, "stopId=" + stopId + "; " + "stopName=" +
                                    stopName + "; " + "stopDistance=" + stopDistance);
                        }

                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray stops) {
                    // If the response is JSONArray instead of expected JSONObject
                }
            });
        }

        private static void getArrivalTimes(String query) throws JSONException {

            String arrivalQuery;

            if (query.contentEquals("s_neu")) {
                arrivalQuery = S_PREDICTIONS_BY_NEU;
            }
            else if (query.contentEquals("s_pru")) {
                arrivalQuery = S_PREDICTIONS_BY_PRU;
            }
            else {
                throw new RuntimeException("Invalid getArrivalTimes parameters.");
            }

            MBTARestClient.get(arrivalQuery, null, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject jsonObject) {
                    Log.d(TAG, "getArrivalTimes: OnSuccess with jsonObject...");

                    String stopName;

                    try {
                        stopName = jsonObject.getString("stop_name");

                        JSONArray trips = jsonObject.getJSONArray("mode").getJSONObject(0).getJSONArray("route")
                        .getJSONObject(0).getJSONArray("direction").getJSONObject(0).getJSONArray("trip");

                        for (int i = 0 ; i < trips.length(); i++) {
                            JSONObject trip = trips.getJSONObject(i);

                            String tripName = trip.getString("trip_name");
                            String arrivalTime = trip.getString("sch_arr_dt");

                            long aTime = Long.parseLong(arrivalTime);

                            tripsWithTimes.put(tripName,aTime);

                            Log.d(TAG, "STOP="+stopName + " | " + "tripName=" + tripName + "; " + "arrivalTime=" +
                                    arrivalTime);

                            //long etaMins = epochDiffToTime(aTime);

                            //Log.d(TAG, "ETA: " + etaMins + " min");
                        }

                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray stops) {
                    // If the response is JSONArray instead of expected JSONObject
                    Log.d(TAG, "getArrivalTimes: OnSuccess not parsed...");
                }
            });
        }
    }
}