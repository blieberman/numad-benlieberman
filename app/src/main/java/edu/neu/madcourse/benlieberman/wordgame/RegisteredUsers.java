package edu.neu.madcourse.benlieberman.wordgame;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import edu.neu.madcourse.benlieberman.R;

public class RegisteredUsers extends Activity implements View.OnClickListener {

    private final String TAG  = "REGISTERED_USERS";
    private RegisteredUsersAdaptor registeredUsersAdaptor;
    private ListView listView;
    private TextView title;
    protected ProgressDialog dialog;
    String userName;
    String playerRegId;
    Boolean isObservation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incognito_list);

        // check intent for observation mode
        isObservation = getIntent().getExtras().getBoolean(CommunicationConstants.OBSERVATION_MODE);

        userName = CommunicationTasks.GetAccountName(RegisteredUsers.this);

        // Initialize the subclass of ParseQueryAdapter
        registeredUsersAdaptor = new RegisteredUsersAdaptor(this);

        // Initialize ListView and set initial view to mainAdapter
        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(registeredUsersAdaptor);
        playerRegId = null;

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseObject parseObject = (ParseObject) listView.getItemAtPosition(position);
                final String opponentName = parseObject.getString("playerName");
                playerRegId = parseObject.getString("regId");

                Log.d(TAG, "onItemClick: player clicked is " + opponentName);
                if (opponentName.equals(userName)) {
                    Toast.makeText(RegisteredUsers.this, "Error, you have selected yourself...",
                            Toast.LENGTH_SHORT).show();
                    // CommunicationTasks.sendMessage("Hello", regId, getApplicationContext());
                }
                else {

                    final ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");
                    query.whereEqualTo("playerName", userName);
                    query.whereEqualTo("opponentName", opponentName);
                    query.whereEqualTo("isSpectatorMode", isObservation);

                    query.getFirstInBackground(new GetCallback<ParseObject>() {
                        public void done(ParseObject object, ParseException e) {
                            if (object == null) {
                                Log.d(TAG, "The Games request does not previously exist | failed.");
                                new SendPlayerInvites().execute(userName, opponentName, playerRegId); // Send invitation
                            } else {
                                Log.d(TAG, "Retrieved the Games object && duplicate");
                                Toast.makeText(RegisteredUsers.this, "Error, already game in progress with this user...",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

        // Initilize TextView for title
        title = (TextView) findViewById(R.id.title);
        title.setText("Registered Users");
    }

    @Override
    public void onClick(View v) {

    }

    private class RegisteredUsersAdaptor extends ParseQueryAdapter<ParseObject> {

        private static final String PARSE_OBJECT = "Registered_Users";

        public RegisteredUsersAdaptor(Context context) {
            // Use the QueryFactory to construct a PQA
            super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {
                public ParseQuery create() {
                    ParseQuery query = new ParseQuery(PARSE_OBJECT);
                    query.orderByDescending("playerName");
                    return query;
                }
            });
        }

        // Customize the layout by overriding getItemView
        @Override
        public View getItemView(ParseObject object, View v, ViewGroup parent) {
            if (v == null) {
                v = View.inflate(getContext(), R.layout.activity_registered_users, null);
            }

            super.getItemView(object, v, parent);

            // Add the player name view
            TextView titleTextView = (TextView) v.findViewById(R.id.wg_ru_playerName);
            titleTextView.setText(object.getString("playerName"));

            return v;
        }
    }

    private class SendPlayerInvites extends AsyncTask<String, Void, Void> {
        String gameID;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(RegisteredUsers.this);
            dialog.setMessage("Processing new game request...");
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            int boolInt = (isObservation) ? 1 : 0;
            int i = params[0].length() * params[1].length() + boolInt + params[0].hashCode();
            gameID = i+"";

            // Send initial game data to cloud
            ParseObject parseObject = new ParseObject("Games");
            parseObject.put("isSpectatorMode", isObservation);
            parseObject.put("playerName", params[0]);
            parseObject.put("opponentName", params[1]);
            parseObject.put("data", "");
            parseObject.put("turnUser", params[1]);
            parseObject.put("gameID", gameID);

            parseObject.saveInBackground();

            Log.d(TAG, "GAMEID = "+ gameID);
            return null;
        }

        @Override
        protected void onPostExecute(Void success) {
            // send notification to opponent
            final String message = "Play with " + userName;

            final ParseQuery<ParseObject> query = ParseQuery.getQuery("Registered_Users");
            query.whereEqualTo("playerName", userName);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                public void done(ParseObject object, ParseException e) {
                    if (object == null) {
                        Log.d("getRegIdFromCloud", "The getFirst request failed.");
                    } else {
                        Log.d("getRegIdFromCloud", "Retrieved the object.");
                        final String inviterRegId = object.getString("regId");
                        CommunicationTasks.sendInvite(message, playerRegId,
                                inviterRegId, gameID, RegisteredUsers.this, isObservation);
                    }
                }
            });

            // dismiss dialog
            if (dialog.isShowing()) {
                dialog.dismiss();
            }

            Intent intent;
            if (isObservation) {
                intent = new Intent(getApplicationContext(), ObservationGame.class);
            }
            else {
                intent = new Intent(getApplicationContext(), TwoPlayerGame.class);
            }
            intent.putExtra("KIND", "invite");
            intent.putExtra(CommunicationConstants.OBSERVATION_MODE, isObservation);
            intent.putExtra("GAMEID", gameID);
            startActivity(intent);
        }
    }

}