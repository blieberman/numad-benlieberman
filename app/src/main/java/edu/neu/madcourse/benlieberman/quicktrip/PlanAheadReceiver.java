package edu.neu.madcourse.benlieberman.quicktrip;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

public class PlanAheadReceiver extends WakefulBroadcastReceiver {
    private static final String TAG = "PlanAheadReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        // Check if alarm has been set -- from SharedPreferences
        SharedPreferences sp;
        sp = context.getSharedPreferences("PlanAhead", Context.MODE_MULTI_PROCESS);
        boolean isAlarmSet = sp.getBoolean("alarm_set_flag", false);

        Log.d(TAG, "isAlarmSet=" + isAlarmSet);
        Log.d(TAG, "onReceive: Broadcast received...");

        // PUSH TOAST MESSAGE
        Toast toast = Toast.makeText(context, "Time to leave!", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

        // VIBRATE NOTIFICATION
        Vibrator v;
        v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (v != null) {
            v.vibrate(400);
        }

        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("alarm_set_flag", false); // value to store
        editor.commit();

        // SEND NOTIFICATION
        ComponentName comp = new ComponentName(context.getPackageName(), PlanAheadService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}