package edu.neu.madcourse.benlieberman;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;

import edu.neu.madcourse.benlieberman.quicktrip.*;
import edu.neu.madcourse.benlieberman.sudoku.Sudoku;
import edu.neu.madcourse.benlieberman.wordgame.CommunicationConstants;
import edu.neu.madcourse.benlieberman.wordgame.RegisteredUsers;
import edu.neu.madcourse.benlieberman.wordgame.WordGame;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(edu.neu.madcourse.benlieberman.R.layout.activity_main);
        String mainTitle = getResources().getString(edu.neu.madcourse.benlieberman.R.string.developer_name);
        setTitle(mainTitle);
    }

    /** openAbout(): Opens the about activity */
    public void openAbout(View view) {
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }

    /** openSudoku(): Opens the main sudoku activity */
    public void openSudoku(View view) {
        Intent intent = new Intent(this, Sudoku.class);
        startActivity(intent);
    }

    /** openTestDictionary(): Opens the main test dictionary activity */
    public void openTestDictionary(View view) {
        Intent intent = new Intent(this, TestDictionary.class);
        startActivity(intent);
    }

    /** openWordGame(): Opens the main word game activity */
    public void openWordGame(View view) {
        Intent intent = new Intent(this, WordGame.class);
        startActivity(intent);
    }

    /** openCommunication(): Opens the main communication activity */
    public void openCommunication(View view) {
        Intent intent = new Intent(this, Communication.class);
        startActivity(intent);
    }

    /** openTwoPlayerGame(): Opens the main communication activity */
    public void openTwoPlayerGame(View view) {
        Intent intent = new Intent(this, RegisteredUsers.class);
        intent.putExtra(CommunicationConstants.OBSERVATION_MODE, false);
        startActivity(intent);
    }


    /** openTrickiestPart(): Opens the main communication activity */
    public void openTrickiestPart(View view) {
        Intent intent = new Intent(this, TrickiestPart.class);
        startActivity(intent);
    }
    /** openFinalProject(): Opens the main communication activity */
    public void openFinalProject(View view) {
        Intent intent = new Intent(this, FinalProjectLanding.class);
        startActivity(intent);
    }

    /** exit(): Hard exits the application with return code 0 */
    public void exit(View view) {
        //System.exit(0);
        finish();
    }

    /** genError(): This function will always cause an exception to be thrown and the app to crash */
    public void genError() {
        return;
    }
}