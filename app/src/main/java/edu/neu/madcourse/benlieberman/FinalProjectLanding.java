package edu.neu.madcourse.benlieberman;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import edu.neu.madcourse.benlieberman.quicktrip.*;

public class FinalProjectLanding extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_project_landing);

        // Run App
        View runAppButton = findViewById(R.id.fp_ra);
        runAppButton.setOnClickListener(this);

        // Acknowledgements
        View ackButton = findViewById(R.id.fp_ack);
        ackButton.setOnClickListener(this);

        // Description
        TextView description = (TextView) findViewById(R.id.fp_description);
        description.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fp_ra:
                openFinalProject();
                break;
            case R.id.fp_ack:
                openAcknowledgements();
                break;
        }
    }

    public void openFinalProject() {
        Intent intent = new Intent(this, QuickTrip.class);
        startActivity(intent);
    }

    public void openAcknowledgements() {
        Intent intent = new Intent(this, edu.neu.madcourse.benlieberman.quicktrip.Acknowledgements.class);
        startActivity(intent);
    }
}