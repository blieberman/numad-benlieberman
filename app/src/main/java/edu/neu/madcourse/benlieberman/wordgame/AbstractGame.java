package edu.neu.madcourse.benlieberman.wordgame;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.view.View.OnTouchListener;
import android.widget.Toast;

import com.parse.ParseObject;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import edu.neu.madcourse.benlieberman.R;

public abstract class AbstractGame extends ActionBarActivity implements OnTouchListener {

    private static final String TAG = "WFGame";
    static public int score;
    public int bonusScore;
    protected HashMap<String, CountDownTimer> wordTimers;
    public TileHolder tileHolder;
    static public Board board;
    static public TextView scoreText;
    public static final int NUMSTARTTILES = 21;
    protected Vibrator vibrator;

    protected MenuItem split_button;
    protected MenuItem dump_button;

    protected int tilesEliminated;

    // DICTIONARY //
    public static DatabaseHelper dbHelper;
    protected ProgressDialog dialog;
    ////////////////

    protected final int[] POINTS = new int[]{
            1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3,  // A-M
            1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10 // N-Z
    };
    protected final int[] TILE_FREQUENCY = new int[]{
            9, 2, 2, 4, 12, 2, 3, 2, 9, 1, 1, 4, 2,  // A-M
            6, 8, 2, 1, 6, 4, 6, 4, 2, 2, 1, 2, 1 // N-Z
    };
    protected final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    protected static Map<Character, Integer> letterToPoint = new HashMap<>();
    protected Map<Character, Integer> letterToFrequency = new HashMap<>();

    public ArrayList<Tile> allTiles = new ArrayList<>();

    protected static final long WORD_TIMER_LENGTH = 15000; // in MS
    protected static final int DEFAULT_WORD_ALPHA = 255;

    private class DictionaryPrep extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(AbstractGame.this);
            dialog.setMessage("First time initialization...");
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            dbHelper = new DatabaseHelper(getApplicationContext());
            try {
                dbHelper.databaseInit();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final Void success) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_game_entire);

        new DictionaryPrep().execute();

        scoreText = (TextView) findViewById(R.id.wg_board_infobar_score);
        tileHolder = (TileHolder) findViewById(R.id.tileholder);
        board = (Board) findViewById(R.id.board);

        board.setOnTouchListener(this);
        tileHolder.setOnTouchListener(this);

        vibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumeWordTimers();
        Music.play(this, R.raw.lifeinabox);
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseWordTimers();
        Music.stop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            dbHelper.close();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_word_game, menu);

        split_button = menu.findItem(R.id.wg_split);
        dump_button = menu.findItem(R.id.wg_dump);

        split_button.setVisible(false);
        dump_button.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.wg_settings) {
            Intent i = new Intent(this, Prefs.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.wg_split) {
            doSplit();
            setMenuStatus();
            return true;
        }
        if (id == R.id.wg_dump) {
            doDump();
            setMenuStatus();
            return true;
        }
        if (id == R.id.wg_quit) {
            gameOver();
            return true;
        }
        if (id == R.id.wg_pause) {
            openPause();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void gameOver() {
        int finalscore = score + bonusScore;
        Toast gameOverNotif;

        gameOverNotif = Toast.makeText(getApplicationContext(), "Game Over. Your score is: " + finalscore,
                Toast.LENGTH_LONG);

        gameOverNotif.setGravity(Gravity.CENTER, 0, 0);
        gameOverNotif.show();

        String userName = CommunicationTasks.GetAccountName(this);

        ParseObject gameScore = new ParseObject("Game_Score");
        gameScore.put("score", finalscore+"");
        gameScore.put("playerName", userName);
        gameScore.put("SinglePlayer", true);

        gameScore.saveInBackground();

        Intent gameSummary = new Intent(this, WordGame.class);
        startActivity(gameSummary);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            //initWordTimer();
        } else {
            //stopWordTimer();
        }
        super.onWindowFocusChanged(hasFocus);
    }

    protected void openPause() {
        Intent i = new Intent(this, PauseMenu.class);
        startActivity(i);
    }

    protected void init() {
        initTiles();
        initTileHolder(NUMSTARTTILES);
        wordTimers = new HashMap<>();
        score = 0;
        setScoreText();
        tilesEliminated = 0;
    }

    protected void initTiles() {
        char[] letters = ALPHABET.toCharArray();
        frequencyMapper(TILE_FREQUENCY, letterToFrequency, letters);
        pointMapper(POINTS, letterToPoint, letters);

        for (char l : letters) {
            int freq = letterToFrequency.get(l);
            int point = letterToPoint.get(l);
            for (int i = 0; i < freq; i++) {
                allTiles.add(new Tile(l, point));
            }
        }
    }

    protected void initTileHolder(int holderSize) {
        for (int i = 1; i <= holderSize; i++) {
            Collections.shuffle(allTiles);
            Tile t = allTiles.remove(0);   // Will remove the first elem
            tileHolder.addTiles(t);
        }
    }

    protected void pointMapper(int[] p, Map<Character, Integer> ltp, char[] letters) {
        if (p == null | p.length != letters.length) {
            throw new IllegalArgumentException("Point frequency length is not equal to number of" +
                    "letters in given alphabet");
        } else {
            for (int i = 0; i < letters.length; i++) {
                ltp.put(letters[i], p[i]);
            }
        }
    }

    public void frequencyMapper(int[] tf, Map<Character, Integer> ltf, char[] letters) {
        if (tf == null | tf.length != letters.length) {
            throw new IllegalArgumentException("Tile frequency length is not equal to number of" +
                    "letters in given alphabet");
        } else {
            for (int i = 0; i < letters.length; i++) {
                ltf.put(letters[i], tf[i]);
            }
        }
    }

    public void doSplit() {
        int n = tileHolder.numTiles();
        if (n < 21) {
            Collections.shuffle(allTiles);
            Tile t = allTiles.remove(0);
            tileHolder.addTiles(t);
        } else {
            return;
        }

        if (n <= 20 && n >= 11) {
            bonusScore += -5;
        } else if (n <= 10 && n >= 5) {
            bonusScore += -0;
        } else if (n == 4) {
            bonusScore += -4;
        } else if (n == 3) {
            bonusScore += -6;
        } else if (n == 2) {
            bonusScore += -8;
        } else if (n == 1) {
            bonusScore += -10;
        } else if (n == 0) {
            bonusScore += -15;
        }

        setScoreText();

        tileHolder.invalidate();
        scoreText.invalidate();
    }

    protected void setMenuStatus() {
        int n = tileHolder.numTiles();

        setDumpVisibility(n);
        setSplitVisibility(n);
    }

    protected void setDumpVisibility(int n) {
        if (n <= 18) {
            dump_button.setVisible(true);
        } else {
            dump_button.setVisible(false);
        }
    }

    protected void setSplitVisibility(int n) {
        if (n <= 20) {
            split_button.setVisible(true);
        } else {
            split_button.setVisible(false);
        }
    }

    public void doDump() {
        int n = tileHolder.numTiles();
        int scoreAcc = 0;

        if (n <= 18) {
            Collections.shuffle(allTiles);
            for (int i = 0; i < 3; i++) {
                Tile t = allTiles.remove(0);
                tileHolder.addTiles(t);
                scoreAcc += t.getValue();
            }
            bonusScore = -((scoreAcc * 2) + 5);
        }

        setScoreText();

        tileHolder.invalidate();
        scoreText.invalidate();
    }

    public void setScoreText() {
        score = score + bonusScore;
        scoreText.setText("Score: " + score);

        if (score < 0) {
            gameOver();
        }
    }

    public void pauseWordTimers() {
        for (CountDownTimer c : wordTimers.values()) {
            c.cancel();
        }
    }

    public void resumeWordTimers() {

        for (Map.Entry<String, ArrayList<int[]>> entry : board.wordsCreated.entrySet()) {
            String key = entry.getKey();
            ArrayList<int[]> value = entry.getValue();

            for (int[] posn : value) {
                int boardIndex = posn[1] * 9 + posn[0];
                if (board.tilesInPlay[boardIndex] != null) {
                    board.tilesInPlay[boardIndex].alphaNum = DEFAULT_WORD_ALPHA;
                }
            }

            wordTimers.get(key).start();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        setMenuStatus();

        if (event.getAction() != MotionEvent.ACTION_DOWN)
            return super.onTouchEvent(event);

        switch (view.getId()) {

            case R.id.board:
                board.select((int) (event.getX() / board.width),
                        (int) (event.getY() / board.height));

                vibrator.vibrate(40);

                if (tileHolder.isTileSection) { // HOLDER TO BOARD MOVE
                    if (isBoardEmpty()
                            || (board.getTile(board.selX, board.selY) == null
                            && board.isAdjacent(board.selX, board.selY)
                            && tileHolder.selectedTile != null)) { // if the board spot is empty
                        // add tile to board queue
                        if (tileHolder.selectedTile != null) {
                            tileHolder.selectedTile.alphaNum = DEFAULT_WORD_ALPHA;
                        }

                        board.setTile(board.selX, board.selY, tileHolder.selectedTile);
                        tileHolder.removeTiles(this.tileHolder.selectedTile); // remove tile from holder

                        board.getWords();
                        setWords();

                        tileHolder.selectedTile = null;
                    }
                    tileHolder.isTileSection = false;
                    tileHolder.invalidate();
                    board.invalidate();
                    return true;

                } else if (board.isBoardSelection && board.tileSelection != null && board.tileSelection.isMovable) { // BOARD TO BOARD MOVE
                    if (board.getTile(board.selX, board.selY) == null) { // you're trying to select a tile on board

                        board.setTile(board.selX, board.selY, board.tileSelection);
                        board.removeTile(board.prevCords);

                        board.getWords();
                        setWords();

                        board.isBoardSelection = false;
                        tileHolder.isTileSection = false;
                        board.invalidate();
                        tileHolder.invalidate();
                        return true;
                    }
                }

                board.tileSelection = board.getTile(board.selX, board.selY);
                board.setPrevCords();
                // just free space on board
                board.isBoardSelection = true;
                tileHolder.isTileSection = false;
                tileHolder.invalidate();
                board.invalidate();
                return true;

            case R.id.tileholder:
                vibrator.vibrate(40);

                tileHolder.selectedTile = null;
                tileHolder.select((int) (event.getX() / tileHolder.cWidth),
                        (int) (event.getY() / tileHolder.cHeight));

                if (board.isBoardSelection && board.tileSelection != null
                        && tileHolder.numTiles() <= 20 && board.tileSelection.isMovable) {
                    tileHolder.addTiles(board.tileSelection);
                    board.removeTile(board.prevCords);
                    tileHolder.selectedTile = null;
                    tileHolder.isTileSection = false;
                    board.isBoardSelection = false;

                    board.getWords();
                    setWords();

                    board.invalidate();
                    tileHolder.invalidate();
                    return true;
                }

                tileHolder.isTileSection = true;
                board.isBoardSelection = false;

                board.invalidate();
                tileHolder.invalidate();
                return true;
        }
        return true;
    }

    private boolean isBoardEmpty() {
        return tileHolder.numTiles() == NUMSTARTTILES - tilesEliminated;
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public void restartWordTimer(String word) {
        ArrayList<int[]> myWord = board.wordsCreated.get(word);
        for (int[] posn : myWord) {
            int boardIndex = posn[1] * 9 + posn[0];
            if (board.tilesInPlay[boardIndex] != null) {
                board.tilesInPlay[boardIndex].alphaNum = DEFAULT_WORD_ALPHA;
            }
        }
        wordTimers.get(word).cancel();
        wordTimers.get(word).start();
    }

    public void setWordsCreatedHoriz() {
        // go through horizonal words
        if (!board.horizWords.isEmpty()) {
            for (Map.Entry<String, ArrayList<int[]>> entry : board.horizWords.entrySet()) {
                final String k = entry.getKey();
                final ArrayList<int[]> v = entry.getValue();

                if (!board.wordsCreated.containsKey(k)) {
                    Log.d(TAG, "setWordsCreated: adding " + k);
                    new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100).startTone(
                            ToneGenerator.TONE_PROP_ACK);

                    int[] mixedPos = null;
                    boolean isRemoving = false;

                    // check for tiles that might be in both words
                    for (ArrayList<int[]> wcPosns : board.wordsCreated.values()) {

                        for (int[] wcPosn : wcPosns) {
                            for (int[] hPosn : v) {
                                if (wcPosn[0] == hPosn[0] && wcPosn[1] == hPosn[1]) {
                                    Log.d(TAG, "setWordsCreatedHoriz: " + hPosn[0] + "," + hPosn[1] + "=" + wcPosn[0] + "," + wcPosn[1]);
                                    Log.d(TAG, "setWordsCreatedHoriz: contains " + hPosn[0] + "," + hPosn[1]);
                                    mixedPos = hPosn;
                                    isRemoving = true;
                                    break;
                                }
                            }
                        }

                        if (isRemoving == true) {
                            String word = getKeyByValue(board.wordsCreated, wcPosns);
                            restartWordTimer(word);
                            if (isWordExtension(word, k)) {
                                mixedPos = null;
                            }
                            break;
                        }
                    }

                    board.wordsCreated.put(k, v);
                    final int[] skipPos = mixedPos;

                    // Create timer
                    wordTimers.put(k, new CountDownTimer(SinglePlayerGame.WORD_TIMER_LENGTH, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                            for (int[] p : v) {
                                if (skipPos != null && (p[0] == skipPos[0] && p[1] == skipPos[1])) {
                                    continue;
                                } else {
                                    board.getTile(p[0], p[1]).alphaNum -= 17.5;
                                    board.invalidate();
                                }
                            }
                            if (millisUntilFinished < 5000) {
                                new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100).startTone(
                                        ToneGenerator.TONE_PROP_BEEP2);
                            }
                        }

                        @Override
                        public void onFinish() {
                            // delete the tiles from the board
                            for (int[] p : v) {
                                board.removeTile(p[1] * 9 + p[0]);
                                tilesEliminated += 1;
                            }
                            bonusScore += score;
                            tileHolder.selectedTile = null;
                            tileHolder.isTileSection = false;
                            board.isBoardSelection = false;
                            board.getWords();
                            setWords();

                            board.invalidate();
                            tileHolder.invalidate();
                        }
                    });
                    wordTimers.get(k).start();
                }
            }
        }
    }

    public boolean isWordExtension(String previous, String current) {
        int curLen = current.length();
        int prevLen = previous.length();

        return prevLen + 1 == curLen &&
                current.contains(previous);
    }

    //TODO: FIX EXENSION OF EXISTING WORD RESULTING IN CONTAINS TO FIRE

    public void setWordsCreatedVert() {
        if (!board.vertWords.isEmpty()) {
            // go through vertical words
            for (Map.Entry<String, ArrayList<int[]>> entry : board.vertWords.entrySet()) {
                final String k = entry.getKey();
                final ArrayList<int[]> v = entry.getValue();

                if (!board.wordsCreated.containsKey(k)) {
                    Log.d(TAG, "setWordsCreated: adding " + k);
                    new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100).startTone(
                            ToneGenerator.TONE_PROP_ACK);

                    int[] mixedPos = null;
                    boolean isRemoving = false;

                    for (ArrayList<int[]> wcPosns : board.wordsCreated.values()) {

                        for (int[] wcPosn : wcPosns) {
                            for (int[] vPosn : v) {
                                if (wcPosn[0] == vPosn[0] && wcPosn[1] == vPosn[1]) {
                                    Log.d(TAG, "setWordsCreatedHoriz: " + vPosn[0] + "," + vPosn[1] + "=" + wcPosn[0] + "," + wcPosn[1]);
                                    Log.d(TAG, "setWordsCreatedVert: contains " + vPosn[0] + "," + vPosn[1]);
                                    mixedPos = vPosn;
                                    isRemoving = true;
                                    break;
                                }
                            }
                        }
                        if (isRemoving == true) {
                            String word = getKeyByValue(board.wordsCreated, wcPosns);
                            restartWordTimer(word);
                            if (isWordExtension(word, k)) {
                                mixedPos = null;
                            }
                            break;
                        }
                    }

                    board.wordsCreated.put(k, v);
                    final int[] skipPos = mixedPos;

                    // Create timer
                    wordTimers.put(k, new CountDownTimer(SinglePlayerGame.WORD_TIMER_LENGTH, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            for (int[] p : v) {
                                if (skipPos != null && (p[0] == skipPos[0] && p[1] == skipPos[1])) {
                                    continue;
                                } else {
                                    board.getTile(p[0], p[1]).alphaNum -= 17.5;
                                    board.invalidate();
                                }
                            }
                            if (millisUntilFinished < 5000) {
                                new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100).startTone(
                                        ToneGenerator.TONE_PROP_BEEP2);
                            }
                        }

                        @Override
                        public void onFinish() {
                            // delete the tiles from the board
                            for (int[] p : v) {
                                board.removeTile(p[1] * 9 + p[0]);
                                tilesEliminated += 1;
                            }
                            bonusScore += score;
                            tileHolder.selectedTile = null;
                            tileHolder.isTileSection = false;
                            board.isBoardSelection = false;
                            board.getWords();
                            setWords();

                            board.invalidate();
                            tileHolder.invalidate();
                        }
                    });
                    wordTimers.get(k).start();
                }
            }
        }
    }

    public void setWordsCreated() {
        setWordsCreatedHoriz();
        setWordsCreatedVert();
    }

    public void removeDeadWords() {
        int count = 0;
        for (Iterator<Map.Entry<String, ArrayList<int[]>>>
                     it = board.wordsCreated.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, ArrayList<int[]>> entry = it.next();
            if (!board.horizWords.containsKey(entry.getKey()) &&
                    !board.vertWords.containsKey(entry.getKey())) {
                Log.d(TAG, "removeDeadWords: removing " + entry.getKey());
                it.remove();
                // remove word timer
                wordTimers.get(entry.getKey()).cancel();
                wordTimers.remove(entry.getKey());
                count += 1;
            }
        }

        if (count > 1) {
            gameOver();
        }
    }

    public void setWordsStatus() {
        ArrayList<Integer> bp = new ArrayList<>();
        for (ArrayList<int[]> posns : board.wordsCreated.values()) {
            for (int[] posn : posns) {
                int boardIndex = posn[1] * 9 + posn[0];
                bp.add(boardIndex);
                // mark individual tiles in word for fade
                board.tilesInPlay[boardIndex].isPartOfWord = true;
                //board.tilesInPlay[boardIndex].alphaNum = DEFAULT_WORD_ALPHA;
            }
        }
        for (int i = 0; i < 100; i++) {
            if (!bp.contains(i) && this.board.tilesInPlay[i] != null) {
                board.tilesInPlay[i].isPartOfWord = false;
                board.tilesInPlay[i].alphaNum = DEFAULT_WORD_ALPHA;
                Log.d(TAG, "setWordsNotPart: setting " + i + " false");
            }
        }
    }

    protected void setWords() {
        setWordsCreated();
        removeDeadWords();
        setWordsStatus();

        setScoreText();
        setMenuStatus();
    }

}