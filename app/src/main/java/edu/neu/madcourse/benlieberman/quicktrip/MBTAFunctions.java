package edu.neu.madcourse.benlieberman.quicktrip;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

public class MBTAFunctions {

    private static final String TAG = "MBTASubwayFunctions";

    private static final String API_KEY = "fBkegU3LMES0GwQlYmLKDg";

    // neu subway query
    private static final String S_PREDICTIONS_BY_NEU = "schedulebystop?api_key=" + API_KEY +
            "&stop=" + "place-nuniv" + "&route=" + "Green-E" + "&direction=" + "1" + "&format=json";

    // prudential subway query
    private static final String S_PREDICTIONS_BY_PRU = "schedulebystop?api_key=" + API_KEY +
            "&stop=" + "place-prmnl" + "&route=" + "Green-E" + "&direction=" + "0" + "&format=json";

    // huntington @ ruggles st bus stop
    private static final String B_PREDICTIONS_BY_RUG = "predictionsbystop?api_key=" +
            API_KEY + "&stop" + "=51317" + "&format=json";

    // huntington @ belvidere st bus stop
    private static final String B_PREDICTIONS_BY_BEL = "predictionsbystop?api_key=" +
            API_KEY + "&stop" + "=11388" + "&format=json";

    private static MBTAFunctions sInstance = null;

    public static MBTAFunctions getInstance() {
        return MBTAFunctions.getsInstance();
    }

    public static MBTAFunctions getsInstance() {
        if (sInstance == null) {
            sInstance = new MBTAFunctions();
        }

        return sInstance;
    }

    public void getArrivalTimes(String query, final MBTARestClient.ResultsListener<JSONObject> aListener) {
        String arrivalQuery;

        if (query.contentEquals("s_neu")) {
            arrivalQuery = S_PREDICTIONS_BY_NEU;
        }
        else if (query.contentEquals("s_pru")) {
            arrivalQuery = S_PREDICTIONS_BY_PRU;
        }
        else if (query.contentEquals("b_rug")) {
            arrivalQuery = B_PREDICTIONS_BY_RUG;
        }
        else if (query.contentEquals("b_bel")) {
            arrivalQuery = B_PREDICTIONS_BY_BEL;
        }
        else {
            throw new RuntimeException("Invalid getArrivalTimes parameters.");
        }

        MBTARestClient.get(arrivalQuery, null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess ( int statusCode, Header[] headers, JSONObject jsonObject) {

                if (statusCode == 200) {
                    aListener.onSuccess(jsonObject);
                }

            }

            @Override
            public void onFailure ( int statusCode, Header[] headers, Throwable throwable, JSONObject
                    errorResponse){
                super.onFailure(statusCode, headers, throwable, errorResponse);

                Log.e(TAG, "MBTARestClient: FAILURE!");
            }

        });
    }

}