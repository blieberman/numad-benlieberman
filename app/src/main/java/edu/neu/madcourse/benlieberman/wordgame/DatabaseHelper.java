package edu.neu.madcourse.benlieberman.wordgame;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import edu.neu.madcourse.benlieberman.R;

public class DatabaseHelper extends SQLiteOpenHelper {

    static String DEST_PATH = "/data/data/edu.neu.madcourse.benlieberman/databases/";
    static String DB_NAME = "words.sql";
    static final int DB_VERSION = 1;

    SQLiteDatabase mDb = null;
    Context myContext;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean databaseExist(String dbName) {
        File dbFile = myContext.getDatabasePath(dbName);
        return dbFile.exists();
    }

    public void databaseInit() throws IOException {
        if (!databaseExist(DB_NAME)) {
            mDb = this.getReadableDatabase();
            copyDB();
        }
        else {
            mDb = this.getReadableDatabase();
        }
    }

    public void copyDB() throws IOException {
        Resources res = myContext.getResources();
        InputStream is = res.openRawResource(R.raw.words);
        OutputStream os = new FileOutputStream(DEST_PATH + DB_NAME);

        byte [] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) > 0) {
            os.write(buffer, 0, length);
        }
        is.close();
        os.flush();
        os.close();
    }

    public boolean containsRow (String s) {
        Cursor c;
        String query = "select 1 from words where content = '"+s+"';";

        c = mDb.rawQuery(query, null);
        if (c.getCount() <= 0) {
            c.close();
            return false;
        }
        else {
            c.close();
            return true;
        }
    }

    @Override
    public void close() {
        if (mDb != null) {
            mDb.close();
        }
    }
}