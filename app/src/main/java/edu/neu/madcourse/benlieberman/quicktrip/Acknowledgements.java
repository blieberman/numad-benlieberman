package edu.neu.madcourse.benlieberman.quicktrip;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import edu.neu.madcourse.benlieberman.R;

public class Acknowledgements extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acknowledgements2);

        // Description
        TextView description = (TextView) findViewById(R.id.fp_ap_ack);
        description.setMovementMethod(new ScrollingMovementMethod());
    }

}
