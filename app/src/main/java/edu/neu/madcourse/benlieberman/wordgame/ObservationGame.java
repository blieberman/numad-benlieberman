package edu.neu.madcourse.benlieberman.wordgame;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import edu.neu.madcourse.benlieberman.R;

public class ObservationGame extends Activity {

    private ObservationReceiver receiver;
    LocalBroadcastManager manager;
    public static final String RECEIVE_TEST = "WG.RECEIVE_TEST";
    static String gameID;
    static TextView moveTV;
    static String boardString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observation_game);

        gameID = getIntent().getStringExtra("GAMEID");
        moveTV = (TextView) findViewById(R.id.wg_observation_text);

        manager = LocalBroadcastManager.getInstance(this);
        receiver = new ObservationReceiver(new Handler());
        manager.registerReceiver(receiver, new IntentFilter(RECEIVE_TEST));

    }

    @Override
    protected void onPause() {
        super.onPause();

        manager.unregisterReceiver(receiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        manager.registerReceiver(receiver, new IntentFilter(RECEIVE_TEST));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        manager.unregisterReceiver(receiver);

        // delete the game data entry from Parse.com
        String userName = CommunicationTasks.GetAccountName(this);

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");
        query.whereEqualTo("playerName", userName);
        query.whereEqualTo("isSpectatorMode", true);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (object == null) {
                    Log.d("delGameFromCloud", "The getFirst request failed.");
                } else {
                    Log.d("delGameFrom", "Retrieved the object.");
                    object.deleteInBackground();
                }
            }
        });
    }

    public static class ObservationReceiver extends BroadcastReceiver {

        private final Handler handler; // Handler used to execute code on the UI thread

        public ObservationReceiver(Handler handler) {
            this.handler = handler;
        }

        @Override
        public void onReceive(final Context context, Intent intent) {
            moveTV.invalidate();

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");
            query.whereEqualTo("gameID", gameID);
            ParseObject currGame;

            // Retrieve the object by id
            try {
                currGame = query.getFirst();
                boardString = currGame.getString("data");
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // Post the UI updating code to our Handler
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Opponent has moved tiles...", Toast.LENGTH_SHORT).show();
                    moveTV.setText(boardString);
                    moveTV.invalidate();
                }
            });
        }
    }

}