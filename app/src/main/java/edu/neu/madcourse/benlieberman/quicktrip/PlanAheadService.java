package edu.neu.madcourse.benlieberman.quicktrip;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import edu.neu.madcourse.benlieberman.R;

public class PlanAheadService extends IntentService {
    private static final String TAG = "PlanAheadService";

    public PlanAheadService() {
        super("PlanAheadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sendNotification("Time to leave!");
    }

    private void sendNotification(String s) {
        Log.d(TAG, "sendNotification: Prepping send...");
        NotificationManager paNotificationManager;

        paNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent i = new Intent(this, ShowRoutes.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, i, 0);

        NotificationCompat.Builder paNotificationBuilder =
                new NotificationCompat.Builder(this).setContentTitle("Alarm").
                        setSmallIcon(R.drawable.qt_logo).
                        setStyle(new NotificationCompat.BigTextStyle().bigText(s)).
                        setContentText(s);

        paNotificationBuilder.setContentIntent(pendingIntent);
        paNotificationBuilder.setAutoCancel(true);
        paNotificationManager.notify(1, paNotificationBuilder.build());
        Log.d(TAG, "sendNotification: Sending...");
    }
}