package edu.neu.madcourse.benlieberman.quicktrip;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.benlieberman.R;

public class RouteDetails extends ActionBarActivity {

    private static final String TAG = "RouteDetails";
    DetailsAdaptor showDetailsAdaptor;
    ArrayList<String> detailsArray =  new ArrayList<>();
    String mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setIcon(R.drawable.ic_quicktrip);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Route Details");

        Intent i = getIntent();
        String title = i.getStringExtra("ROUTE_TITLE");
        String type = i.getStringExtra("ROUTE_TYPE");
        String details = i.getStringExtra("ROUTE_DETAILS");
        mode = i.getStringExtra("TRANS_MODE");

        TextView detailType = (TextView) findViewById(R.id.qt_rd_et);
        ListView detailList = (ListView) findViewById(R.id.qt_rd_lv);

        detailType.setText(type);

        Log.d(TAG, "onCreate-- details= " + details);

        detailsArray = stringToArrayList(details);

        // Adaptor for ListView
        showDetailsAdaptor = new DetailsAdaptor(this, detailsArray);
        detailList.setAdapter(showDetailsAdaptor);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quick_trip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        Log.d(TAG, "ActionBar click detected...");
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<String> stringToArrayList(String s) {
        ArrayList<String> ar = new ArrayList<>();
        String lines[] = s.split("\\n");

        for(String line: lines) {
            ar.add(line);
        }

        return ar;
    }

    private class DetailsAdaptor extends ArrayAdapter<String> {

        private ArrayList<String> values;

        public DetailsAdaptor(Context context, ArrayList<String> values) {
            super(context, R.layout.route_list, values);
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View mainView = inflater.inflate(R.layout.routedetails_list, parent, false);

            ImageView imageView = (ImageView) mainView.findViewById(R.id.detailMode);
            TextView detailSummary = (TextView) mainView.findViewById(R.id.detailSummary);

            String[] routeText;
            String rt;
            String rs;

            rs = values.get(position);
            //rt = routeText[0].trim();
            //rs = routeText[1].trim();
            //rs = rs.substring(0, 1).toUpperCase() + rs.substring(1);
            //detailTime.setText(rt);

            detailSummary.setText(rs);

            if (rs.startsWith("Walk")) {
                imageView.setImageResource(R.drawable.walk);
            }
            if (rs.startsWith("Wait")) {
                imageView.setImageResource(R.drawable.wait);
            }
            if (rs.startsWith("Get")) {
                imageView.setImageResource(R.drawable.getoff);
            }
            else if (rs.startsWith("Ride") && mode.contentEquals("subway")) {
                imageView.setImageResource(R.drawable.subway);
            }
            else if (rs.startsWith("Ride") && mode.contentEquals("bus")) {
                imageView.setImageResource(R.drawable.bus);
            }

            return mainView;
        }
    }
}
