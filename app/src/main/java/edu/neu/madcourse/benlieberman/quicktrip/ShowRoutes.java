package edu.neu.madcourse.benlieberman.quicktrip;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import edu.neu.madcourse.benlieberman.R;

public class ShowRoutes extends ActionBarActivity implements OnClickListener {

    private static final String TAG = "ShowRoutes";

    private static ArrayList<String> quickestTrips =  new ArrayList<>();

    private static Map<String, Long> walkTimes = new HashMap<>();
    private static Map<String, Long> waitTimes = new HashMap<>();
    private String rdTitle;

    RoutesAdaptor showRoutesAdaptor;

    /* TRAVEL CONSTANTS */
    //total time between pru and neu green line stops (both ways)
    private static final long MIN_SRIDE_BETWEEN_GREENLINE = 3;
    //total time between pru and neu green line stops (both ways)
    private static final long MIN_BRIDE_BETWEEN_39 = 5;
    // time to walk to neu s stop from wvh
    private static final long MIN_WALK_TO_S_NEU = 5;
    // time to walk to pru s stop from 177
    private static final long MIN_WALK_TO_S_PRU = 2;
    // time to walk to rug b stop from wvh
    private static final long MIN_WALK_TO_B_RUG = 4;
    // time to walk from for b stop to wvh
    private static final long MIN_WALK_TO_B_FORS = 1;
    // time to walk to bel b stop from 177
    private static final long MIN_WALK_TO_B_BEL = 1;

    // time to walk between wvh and 177 huntington (both ways)
    private static final long MIN_WALK_BETWEEN_LOCS = 14;

    private static String subwayTimeText;
    private static String busTimeText;

    TextView routeTitleSum;

    TextView routeInfo;
    ListView routeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_routes);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setIcon(R.drawable.ic_quicktrip);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Travel Now");
        actionBar.setSubtitle("Select a destination");

        // WVH -> 177 Huntington Button
        View toH177Button = findViewById(R.id.final_bt_toh177);
        toH177Button.setOnClickListener(this);

        // 177 Huntington -> WVH
        View toWVHButton = findViewById(R.id.final_bt_towvh);
        toWVHButton.setOnClickListener(this);

        // TextView
        routeInfo = (TextView) findViewById(R.id.final_txt_results);
        // ListView
        routeList = (ListView) findViewById(R.id.final_list_quickroutes);
        // TextView
        routeTitleSum = (TextView) findViewById(R.id.qt_sr_gti);

        // Adaptor for ListView
        showRoutesAdaptor = new RoutesAdaptor(this, quickestTrips);
        routeList.setAdapter(showRoutesAdaptor);
        showRoutesAdaptor.clear();
        routeTitleSum.setText("");

        routeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String route = (String) routeList.getItemAtPosition(position);
                Log.d(TAG, "onItemClick: route clicked is " + route);

                if (!route.isEmpty()) {

                    Intent rdIntent = new Intent(getApplicationContext(), RouteDetails.class);
                    rdIntent.putExtra("ROUTE_TITLE", rdTitle);
                    rdIntent.putExtra("ROUTE_TYPE", route);

                    if (route.contains("from Northeastern")) {
                        rdIntent.putExtra("ROUTE_DETAILS", subwayTimeText);
                        rdIntent.putExtra("TRANS_MODE", "subway");
                        startActivity(rdIntent);
                    }
                    if (route.contains("from Prudential")) {
                        rdIntent.putExtra("ROUTE_DETAILS", subwayTimeText);
                        rdIntent.putExtra("TRANS_MODE", "subway");
                        startActivity(rdIntent);
                    }
                    if (route.contains("MBTA 39 bus")) {
                        rdIntent.putExtra("ROUTE_DETAILS", busTimeText);
                        rdIntent.putExtra("TRANS_MODE", "bus");
                        startActivity(rdIntent);
                    }
                    if (route.contains("walking")) {
                        rdIntent.putExtra("ROUTE_DETAILS", "Walk on Huntington Avenue for 14 min");
                        rdIntent.putExtra("TRANS_MODE", "walk");
                        startActivity(rdIntent);
                    }
                }
            }
        });
    }

    private class RoutesAdaptor extends ArrayAdapter<String> {

        private ArrayList<String> values;

        public RoutesAdaptor(Context context, ArrayList<String> values) {
            super(context, R.layout.route_list, values);
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View mainView = inflater.inflate(R.layout.route_list, parent, false);

            ImageView imageView = (ImageView) mainView.findViewById(R.id.transMode);
            TextView routeTime = (TextView) mainView.findViewById(R.id.totalTime);
            TextView healthScoreText = (TextView) mainView.findViewById(R.id.healthScoreText);
            TextView routeSummary = (TextView) mainView.findViewById(R.id.routeSummary);

            String[] routeText;
            String rt;
            String rs;

            String hsKey = null;

            routeText = values.get(position).split("via");
            rt = routeText[0].trim();
            rs = routeText[1].trim();
            rs = rs.substring(0, 1).toUpperCase() + rs.substring(1);

            routeTime.setText(rt);
            routeSummary.setText(rs);

            if (rs.startsWith("MBTA E-Line")) {
                imageView.setImageResource(R.drawable.subway);
                hsKey = "subway";

            }
            else if (rs.startsWith("MBTA 39 bus")) {
                imageView.setImageResource(R.drawable.bus);
                hsKey = "bus";
            }
            else if (rs.startsWith("Walking")) {
                imageView.setImageResource(R.drawable.walk);
                hsKey = "walk";
            }

            if (position == 0) {
                mainView.setBackgroundColor(Color.rgb(63,124,172));
                routeSummary.setTextColor(Color.WHITE);
                healthScoreText.setTextColor(Color.WHITE);
                routeTime.setTextColor(Color.rgb(216, 255, 214));
            }

            int hs = healthyScore(hsKey);

            for (int i = 1; i <= hs; i++) {
                Log.d(TAG, "getView-- findViewWithTag= " + "hm" + i);
                ImageView iv = (ImageView) mainView.findViewWithTag("hm" + i);
                iv.setImageResource(R.drawable.heart);
            }

            return mainView;
        }
    }

    private int healthyScore(String key) {
        Log.d(TAG, "healthyScore-- key= " + key);

        if (key == null) {
            return 0;
        }
        else if (key.contentEquals("walk")) {
            return 3;
        }

        long walk = walkTimes.get(key);
        long wait = waitTimes.get(key);

        if (wait == 0) {
            wait += 1;
        }

        Log.d(TAG, "healthyScore-- " + walk + "/" + wait);

        long score = walk/wait;

        if (walk < 5) {
            score += -1;
        }

        if (score >=  2) {
           return 3;
        }
        else if (score >= 1) {
            return 2;
        }
        else {
            return 1;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quick_trip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        Log.d(TAG, "ActionBar click detected...");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        quickestTrips.clear();
        walkTimes.clear();
        waitTimes.clear();
        rdTitle = "";

        switch (v.getId()) {
            case R.id.final_bt_toh177:
                //wvhToHQuery();
                rdTitle = "Leave WVH now & arrive at 177 Huntington in...";
                routeTitleSum.setText(rdTitle);
                routeTitleSum.invalidate();
                populateList(MIN_WALK_BETWEEN_LOCS + " minutes via walking on Huntington Ave.");
                fetchTo177SubwayFeed();
                fetchTo177BusFeed();
                break;
            case R.id.final_bt_towvh:
                //hToWvhQuery();
                rdTitle = "Leave 177 Huntington now & arrive at WVH in...";
                routeTitleSum.setText(rdTitle);
                routeTitleSum.invalidate();
                populateList(MIN_WALK_BETWEEN_LOCS + " minutes via walking on Huntington Ave.");
                fetchToWVHSubwayFeed();
                fetchToWVHBusFeed();
                break;
        }
    }

    private void fetchTo177SubwayFeed() {

        MBTAFunctions.getInstance().getArrivalTimes("s_neu", new MBTARestClient.ResultsListener<JSONObject>() {

            Map<String,Long> tripsWithTimes = new TreeMap<>();

            @Override
            public void onSuccess(JSONObject feed) {
                Log.d(TAG, "Feed: " + feed);
                tripsWithTimes = parseSubwayJSON(feed);

                // subway
                String TransKey = calcFirstOption(
                        tripsWithTimes, MIN_WALK_TO_S_NEU);
                long timeShortest = calcRouteTime(TransKey, tripsWithTimes);
                if (timeShortest > 0) {
                    populateList(timeShortest + " minutes via MBTA E-Line from Northeastern University (towards Lechmere)");
                    Log.d(TAG, "fetchFeed -- added route of " + timeShortest);
                }
                showRoutesAdaptor.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable e) {
                Log.e(TAG, "Unable to fetch feed.");
            }
        });
    }

    private void fetchToWVHSubwayFeed() {

        MBTAFunctions.getInstance().getArrivalTimes("s_pru", new MBTARestClient.ResultsListener<JSONObject>() {

            Map<String,Long> tripsWithTimes = new TreeMap<>();

            @Override
            public void onSuccess(JSONObject feed) {
                Log.d(TAG, "Feed: " + feed);
                tripsWithTimes = parseSubwayJSON(feed);

                // subway
                String TransKey = calcFirstOption(
                        tripsWithTimes, MIN_WALK_TO_S_PRU);
                long timeShortest = calcRouteTime(TransKey, tripsWithTimes);
                if (timeShortest > 0) {
                    populateList(timeShortest + " minutes via MBTA E-Line from Prudential (towards Heath St.)");
                    Log.d(TAG, "fetchFeed -- added route of " + timeShortest);
                }
                showRoutesAdaptor.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable e) {
                Log.e(TAG, "Unable to fetch feed.");
            }
        });
    }

    private void fetchTo177BusFeed() {

        MBTAFunctions.getInstance().getArrivalTimes("b_rug", new MBTARestClient.ResultsListener<JSONObject>() {

            Map<String,Long> tripsWithTimes = new TreeMap<>();

            @Override
            public void onSuccess(JSONObject feed) {
                Log.d(TAG, "Feed: " + feed);
                tripsWithTimes = parseBusJSON(feed);

                // bus
                String TransKey = calcFirstOption(
                        tripsWithTimes, MIN_WALK_TO_B_RUG);
                long timeShortest = calcRouteTime(TransKey, tripsWithTimes);
                if (timeShortest > 0) {
                    populateList(timeShortest + " minutes via MBTA 39 bus from Ruggles St. (towards Forest Hills)");
                    Log.d(TAG, "fetchFeed -- added route of " + timeShortest);
                }
                showRoutesAdaptor.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable e) {
                Log.e(TAG, "Unable to fetch feed.");
            }
        });
    }

    private void fetchToWVHBusFeed() {

        MBTAFunctions.getInstance().getArrivalTimes("b_bel", new MBTARestClient.ResultsListener<JSONObject>() {

            Map<String,Long> tripsWithTimes = new TreeMap<>();

            @Override
            public void onSuccess(JSONObject feed) {
                Log.d(TAG, "Feed: " + feed);
                tripsWithTimes = parseBusJSON(feed);

                // bus
                String TransKey = calcFirstOption(
                        tripsWithTimes, MIN_WALK_TO_B_BEL);
                long timeShortest = calcRouteTime(TransKey, tripsWithTimes);
                if (timeShortest > 0) {
                    populateList(timeShortest + " minutes via MBTA 39 bus from Belvidere St. (towards Back Bay)");
                    Log.d(TAG, "fetchFeed -- added route of " + timeShortest);
                }
                showRoutesAdaptor.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable e) {
                Log.e(TAG, "Unable to fetch feed.");
            }
        });
    }

    private Map<String,Long> parseSubwayJSON(JSONObject jsonObject) {
        Map<String,Long> tripsAndTimes = new TreeMap<>();

        try {
            tripsAndTimes.clear();

            JSONArray trips = jsonObject.getJSONArray("mode").getJSONObject(0).getJSONArray("route")
                    .getJSONObject(0).getJSONArray("direction").getJSONObject(0).getJSONArray("trip");


            for (int i = 0; i < trips.length(); i++) {
                JSONObject trip = trips.getJSONObject(i);

                String tripName = trip.getString("trip_name");
                String arrivalTime = trip.getString("sch_arr_dt");

                long aTime = Long.parseLong(arrivalTime);

                tripsAndTimes.put(tripName, aTime);

                for (Map.Entry<String, Long> entry : tripsAndTimes.entrySet()) {
                    Log.d(TAG, entry.getKey() + "/" + entry.getValue());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return tripsAndTimes;
    }

    private Map<String,Long> parseBusJSON(JSONObject jsonObject) {
        Map<String,Long> tripsAndTimes = new TreeMap<>();

        try {
            tripsAndTimes.clear();

            JSONArray trips = jsonObject.getJSONArray("mode").getJSONObject(0).getJSONArray("route")
                    .getJSONObject(0).getJSONArray("direction").getJSONObject(0).getJSONArray("trip");


            for (int i = 0; i < trips.length(); i++) {
                JSONObject trip = trips.getJSONObject(i);

                String tripName = trip.getString("trip_name");
                String arrivalTime = trip.getString("pre_dt");

                long aTime = Long.parseLong(arrivalTime);

                tripsAndTimes.put(tripName, aTime);

                for (Map.Entry<String, Long> entry : tripsAndTimes.entrySet()) {
                    Log.d(TAG, entry.getKey() + "/" + entry.getValue());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return tripsAndTimes;
    }

    private void populateList(String route) {
        quickestTrips.add(route);
        Collections.sort(quickestTrips, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                s1 = getFirstWord(s1);
                s2 = getFirstWord(s2);

                Integer i1 = Integer.parseInt(s1);
                Integer i2 = Integer.parseInt(s2);

                if (i1 == i2)
                    return 0;
                if (i1 == null)
                    return -1;
                if (i2 == null)
                    return 1;

                return i1.compareTo(i2);
            }
        });
    }

    private String getFirstWord(String text) {
        if (text.indexOf(' ') > -1) {
            return text.substring(0, text.indexOf(' '));
        } else {
            return text;
        }
    }

    // given a map of vehicles arrivals & time away, returns catchable vehicle option
    private static String calcFirstOption(Map<String,Long> map, long timeAway) {
        for (Map.Entry<String, Long> entry : map.entrySet()) {
            String key = entry.getKey();
            long arrivalTime = entry.getValue();
            long etaMins = epochDiffToTime(arrivalTime);

            if (etaMins >= timeAway) {
                Log.d(TAG, "calcFirstOption-- trans:" + key + "| eta:" +etaMins + " min");
                return key;
            }
        }
        return "";
    }

    // given a quickest type of trip and a list of trips, return the total time of the route
    private static long calcRouteTime(String key, Map<String,Long> map) {
        if (!key.isEmpty()) {
            long arrivalTime = map.get(key);
            long etaMins = epochDiffToTime(arrivalTime);

            //// subway ////
            if (key.contains("Heath Street to Lechmere")) {
                // is from WVH to 177 via Subway @ NEU
                long totalTime = MIN_WALK_TO_S_NEU + (etaMins - MIN_WALK_TO_S_NEU)
                        + MIN_SRIDE_BETWEEN_GREENLINE + MIN_WALK_TO_S_PRU;

                long waitTime = etaMins - MIN_WALK_TO_S_NEU;
                long walkTime = MIN_WALK_TO_S_NEU + MIN_WALK_TO_S_PRU;

                waitTimes.put("subway", waitTime);
                walkTimes.put("subway", walkTime);

                subwayTimeText =
                        "Walk " + MIN_WALK_TO_S_NEU + " min " + "to NEU Green Line stop (towards Lechmere)\n" +
                                "Wait " + (etaMins - MIN_WALK_TO_S_NEU) + " min for subway\n" +
                                "Ride subway for " + MIN_SRIDE_BETWEEN_GREENLINE + " min\n" +
                                "Get off subway at Prudential stop\n" +
                                "Walk " + MIN_WALK_TO_S_PRU + " min to 177 Huntington";

                Log.d(TAG, "calcRouteTime-- ROUTE TOTAL TIME:" + totalTime);
                return totalTime;
            }
            if (key.contains("Inbound to Heath Street")) {
                // is from 177 to WVH via Subway @ PRU
                long totalTime = MIN_WALK_TO_S_PRU + (etaMins - MIN_WALK_TO_S_PRU)
                        + MIN_SRIDE_BETWEEN_GREENLINE + MIN_WALK_TO_S_NEU;

                long waitTime = etaMins - MIN_WALK_TO_S_PRU;
                long walkTime = MIN_WALK_TO_S_PRU + MIN_WALK_TO_S_NEU;

                waitTimes.put("subway", waitTime);
                walkTimes.put("subway", walkTime);

                subwayTimeText =
                        "Walk " + MIN_WALK_TO_S_PRU + " min " + "to PRU Green Line stop (towards Heath Street)\n" +
                                "Wait " + (etaMins - MIN_WALK_TO_S_PRU) + " min for subway\n" +
                                "Ride subway for " + MIN_SRIDE_BETWEEN_GREENLINE + " min\n" +
                                "Get off subway at Northeastern University stop\n" +
                                "Walk " + MIN_WALK_TO_S_NEU + " min to WVH";

                Log.d(TAG, "calcRouteTime-- ROUTE TOTAL TIME:" + totalTime);
                return totalTime;
            }

            //// bus ////
            if (key.contains("Forest Hills Station to Back Bay Station")) {
                // is from WVH to 177 via Bus @ Ruggles
                long totalTime = MIN_WALK_TO_B_RUG + (etaMins - MIN_WALK_TO_B_RUG)
                        + MIN_BRIDE_BETWEEN_39 + MIN_WALK_TO_B_BEL;

                long waitTime = etaMins - MIN_WALK_TO_B_RUG;
                long walkTime = MIN_WALK_TO_B_RUG + MIN_WALK_TO_B_BEL;

                waitTimes.put("bus", waitTime);
                walkTimes.put("bus", walkTime);

                busTimeText =
                        "Walk " + MIN_WALK_TO_B_RUG + " min " + "to Huntington Ave @ Ruggles St. stop\n" +
                                "Wait " + (etaMins - MIN_WALK_TO_B_RUG) + " min for bus\n" +
                                "Ride bus for " + MIN_BRIDE_BETWEEN_39 + " min\n" +
                                "Get off bus at Huntington Ave @ Belvidere St. stop\n" +
                                "Walk " + MIN_WALK_TO_B_BEL + " min to 177 Huntington";

                Log.d(TAG, "calcRouteTime-- ROUTE TOTAL TIME:" + totalTime);
                return totalTime;
            }
            if (key.contains("Back Bay Station to Forest Hills Station")) {
                // is from 177 to WVH via Bus @ Belvidere
                long totalTime = MIN_WALK_TO_B_BEL + (etaMins - MIN_WALK_TO_B_BEL)
                        + MIN_BRIDE_BETWEEN_39 + MIN_WALK_TO_B_FORS;

                long waitTime = etaMins - MIN_WALK_TO_B_BEL;
                long walkTime = MIN_WALK_TO_B_BEL + MIN_WALK_TO_B_FORS;

                waitTimes.put("bus", waitTime);
                walkTimes.put("bus", walkTime);

                busTimeText =
                        "Walk " + MIN_WALK_TO_B_BEL + " min " + "to Huntington Ave @ Belvidere St. stop\n" +
                                "Wait " + (etaMins - MIN_WALK_TO_B_BEL) + " min for bus\n" +
                                "Ride bus for " + MIN_BRIDE_BETWEEN_39 + " min\n" +
                                "Get off bus at Huntington Ave @ Forsyth Way stop\n" +
                                "Walk " + MIN_WALK_TO_B_FORS + " min to WVH";

                Log.d(TAG, "calcRouteTime-- ROUTE TOTAL TIME:" + totalTime);
                return totalTime;
            }

        }
        return 0;
    }

    // given an epoch, returns the difference between the current date in minutes
    private static long epochDiffToTime(long until) {
        long diffInMillis;

        //format until epoch to date
        Date untilD = new Date(until * 1000);

        //get system date
        Date currD = new Date();

        // difference in minutes
        diffInMillis = getDateDiff(currD, untilD, TimeUnit.MINUTES);
        return diffInMillis;
    }

    // helper function for epochDiffToTime;
    // returns millisecond difference in two given dates
    private static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillis = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillis, TimeUnit.MILLISECONDS);
    }

}