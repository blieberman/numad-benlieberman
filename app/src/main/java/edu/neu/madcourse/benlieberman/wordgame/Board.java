package edu.neu.madcourse.benlieberman.wordgame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import edu.neu.madcourse.benlieberman.R;

public class Board extends View {

    private static final String TAG = "WFBoard";

    private static final String SELX = "selX";
    private static final String SELY = "selY";
    private static final String VIEW_STATE = "viewState";

    public float width;    // width of one tile
    public float height;   // height of one tile
    public int selX;       // X index of selection
    public int selY;       // Y index of selection
    public int prevX;
    public int prevY;

    public final Rect selRect = new Rect();
    public boolean isBoardSelection = false;
    public Tile tileSelection;
    public Map<String, ArrayList<int[]>> wordsCreated = new HashMap<>();
    public int prevCords = 0;
    public Tile[] tilesInPlay;

    public Map<String, ArrayList<int[]>> horizWords = new HashMap<>();  // { "ben",{ [1,1],[1,2],[1,3]} }
    public Map<String, ArrayList<int[]>> vertWords = new HashMap<>();


    public Board(Context context) {
        super(context);

        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public Board(Context context, AttributeSet attrs) {
        super(context, attrs);
        tilesInPlay = new Tile[10 * 10];
        tileSelection = null;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable p = super.onSaveInstanceState();
        Log.d(TAG, "onSaveInstanceState");
        Bundle bundle = new Bundle();
        bundle.putInt(SELX, selX);
        bundle.putInt(SELY, selY);
        bundle.putParcelable(VIEW_STATE, p);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Log.d(TAG, "onRestoreInstanceState");
        Bundle bundle = (Bundle) state;
        select(bundle.getInt(SELX), bundle.getInt(SELY));
        super.onRestoreInstanceState(bundle.getParcelable(VIEW_STATE));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        width = w / 9f;
        height = h / 9f;
        getRect(selX, selY, selRect);
        Log.d(TAG, "onSizeChanged: width " + width + ", height "
                + height);
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // Draw the background...
        Paint background = new Paint();
        background.setColor(Color.WHITE);
        canvas.drawRect(0, 0, getWidth(), getHeight(), background);

        // Draw the board...
        // Define colors for the grid lines
        Paint dark = new Paint();
        dark.setColor(getResources().getColor(R.color.puzzle_dark));

        Paint hilite = new Paint();
        hilite.setColor(getResources().getColor(R.color.puzzle_hilite));

        Paint light = new Paint();
        light.setColor(getResources().getColor(R.color.puzzle_light));

        Paint valid = new Paint();
        valid.setColor(Color.GREEN);
        valid.setStyle(Paint.Style.STROKE);

        // Draw the minor grid lines
        for (int i = 0; i < 9; i++) {
            canvas.drawLine(0, i * height, getWidth(), i * height,
                    light);
            canvas.drawLine(0, i * height + 1, getWidth(), i * height
                    + 1, hilite);
            canvas.drawLine(i * width, 0, i * width, getHeight(),
                    light);
            canvas.drawLine(i * width + 1, 0, i * width + 1,
                    getHeight(), hilite);
        }

        // Draw tiles in play
        if (tilesInPlay != null) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (tilesInPlay[i * 9 + j] != null && !tilesInPlay[i * 9 + j].isPartOfWord) {
                        tilesInPlay[i * 9 + j].drawTile2(this.getContext(), canvas,
                                (int) width, (int) height,
                                (int) (j * width),
                                (int) (i * height));
                    }
                    if (tilesInPlay[i * 9 + j] != null && tilesInPlay[i * 9 + j].isPartOfWord) {
                        tilesInPlay[i * 9 + j].drawTile1(this.getContext(), canvas,
                                (int) width, (int) height,
                                (int) (j * width),
                                (int) (i * height));
                    }
                }
            }
        }

        // Draw board selection
        if (isBoardSelection) {
            // Draw the selection...
            Log.d(TAG, "selRect=" + selRect);
            Paint selected = new Paint();
            selected.setColor(getResources().getColor(R.color.puzzle_selected));
            canvas.drawRect(selRect, selected);
        }

        // Draw board word validations
        if (!horizWords.isEmpty()) {
            for (ArrayList<int[]> value : horizWords.values()) {
                for (int[] posn : value) {
                    int posX = posn[0];
                    int posY = posn[1];
                    Rect validRect = new Rect();

                    //Log.d(TAG, "DRAWHORIZWORDS: x " + posX + ", y " + posY);
                    getRect(posX, posY, validRect);
                    canvas.drawRect(validRect, valid);
                }
            }
        }
        if (!vertWords.isEmpty()) {
            for (ArrayList<int[]> value : vertWords.values()) {
                for (int[] posn : value) {
                    int posX = posn[0];
                    int posY = posn[1];
                    Rect validRect = new Rect();

                    //Log.d(TAG, "DRAWVERTWORDS: x " + posX + ", y " + posY);
                    getRect(posX, posY, validRect);
                    canvas.drawRect(validRect, valid);
                }
            }
        }
    }

    public void select(int x, int y) {
        invalidate(selRect);
        selX = Math.min(Math.max(x, 0), 8);
        selY = Math.min(Math.max(y, 0), 8);
        getRect(selX, selY, selRect);
        Log.d(TAG, "onTouchEvent: x " + selX + ", y " + selY);
        invalidate(selRect);
    }

    private void getRect(int x, int y, Rect rect) {
        rect.set((int) (x * width), (int) (y * height), (int) (x
                * width + width), (int) (y * height + height));
    }

    /**
     * Return the tile at the given coordinates
     */
    public Tile getTile(int x, int y) {
        return tilesInPlay[y * 9 + x];
    }

    /**
     * Change the tile at the given coordinates
     */
    public void setTile(int x, int y, Tile tile) {
        tilesInPlay[y * 9 + x] = tile;
    }

    public void setPrevCords() {
        this.prevCords = selY * 9 + selX;
        this.prevX = selX;
        this.prevY = selY;
    }

    public void removeTile(int i) {
        tilesInPlay[i] = null;
    }

    public boolean isAdjacent(int x, int y) {
        if (x == 0 || y == 0) {
            return true;
        }
        return (getTile(x + 1, y) != null) || (getTile(x - 1, y) != null) ||
                (getTile(x, y + 1) != null) || (getTile(x, y - 1) != null);
    }

    public void getHorizontalWords() {
        String string;
        int[] posn;
        ArrayList<int[]> posns;
        int points = 0;

        if (tilesInPlay != null) {
            for (int y = 0; y < 9; y++) {
                string = "";
                points = 0;
                posns = new ArrayList<>();
                for (int x = 0; x < 9; x++) {
                    posn = new int[2];
                    if (getTile(x, y) != null) {
                        string += getTile(x, y).letter;
                        points += getTile(x, y).getValue();
                        posn[0] = x;
                        posn[1] = y;
                        posns.add(posn);
                    }
                    if (getTile(x, y) == null) {
                        string += " ";
                    }
                }
                string = string.trim();
                if (string.length() > 2 && SinglePlayerGame.dbHelper.containsRow(string)
                        && !horizWords.containsKey(string)) {
                    horizWords.put(string, posns);
                    SinglePlayerGame.score += points;
                }
            }

            for (Map.Entry<String, ArrayList<int[]>> entry : horizWords.entrySet()) {
                String key = entry.getKey();
                ArrayList value = entry.getValue();

                Log.d(TAG, "getHorizonalWords: String=" + key + ";"
                        + " Cords:" + value.toString() + "Score: " + points);
            }
        }
    }

    public void getVerticalWords() {
        String string;
        int[] posn;
        ArrayList<int[]> posns;
        int points = 0;

        if (tilesInPlay != null) {
            for (int x = 0; x < 9; x++) {
                string = "";
                points = 0;
                posns = new ArrayList<>();
                for (int y = 0; y < 9; y++) {
                    posn = new int[2];
                    if (getTile(x, y) != null) {
                        string += getTile(x, y).letter;
                        points += getTile(x, y).getValue();
                        posn[0] = x;
                        posn[1] = y;
                        posns.add(posn);
                    }
                    if (getTile(x, y) == null) {
                        string += " ";
                    }
                }
                string = string.trim();
                if (string.length() > 2 && SinglePlayerGame.dbHelper.containsRow(string)) {
                    vertWords.put(string, posns);
                    SinglePlayerGame.score += points;
                }
            }

            for (Map.Entry<String, ArrayList<int[]>> entry : vertWords.entrySet()) {
                String key = entry.getKey();
                ArrayList value = entry.getValue();

                Log.d(TAG, "getVerticalWords: String=" + key + ";"
                        + " Cords:" + value.toString() + "Score: " + points);
            }
        }
    }

    public void getWords() {
        SinglePlayerGame.score = 0;
        clearWordMaps();

        getHorizontalWords();
        getVerticalWords();
    }

    public void clearWordMaps() {
        vertWords.clear();
        horizWords.clear();
    }
}