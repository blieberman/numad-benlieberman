package edu.neu.madcourse.benlieberman.quicktrip;

import com.loopj.android.http.*;

import org.apache.http.HttpVersion;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.params.CoreProtocolPNames;

public class MBTARestClient {
    private static final String BASE_URL =
            "http://realtime.mbta.com/developer/api/v2/";

    private static AsyncHttpClient asyncHttpClient;

    static {
        asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.getHttpClient().getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, false);
        asyncHttpClient.getHttpClient().getParams().setParameter(ClientPNames.HANDLE_REDIRECTS, false);
        asyncHttpClient.getHttpClient().getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
    }

    public static void get(String url, RequestParams params,
                           AsyncHttpResponseHandler responseHandler) {
        asyncHttpClient.get(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    public interface ResultsListener<T> {

        public void onSuccess(T result);
        public void onFailure(Throwable e);

    }
}