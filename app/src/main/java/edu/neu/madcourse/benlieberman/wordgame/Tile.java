package edu.neu.madcourse.benlieberman.wordgame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public class Tile {
    Character letter;
    int value;
    int alphaNum;
    boolean isPartOfWord;
    boolean isMovable;

    public Tile(Character character, int value) {
        this.letter = character;
        this.value = value;
        this.isPartOfWord = false;
        this.isMovable = true;
        this.alphaNum = SinglePlayerGame.DEFAULT_WORD_ALPHA;
    }

    public Character getCharacter() {
        return letter;
    }

    public void setCharacter(Character character) {
        this.letter = character;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void drawTile1(Context context, Canvas canvas, int width, int height, int x, int y) {
        // draw the cards
        int id = context.getResources().getIdentifier(this.getCharacter().toString(), "drawable",
                context.getPackageName());

        Drawable t = context.getResources().getDrawable(id);
        t.mutate();

        t.setAlpha(this.alphaNum);

        t.setBounds(x,y,x+width,y+height);
        t.draw(canvas);
    }

    public void drawTile2(Context context, Canvas canvas, int width, int height, int x, int y) {
        // draw the cards
        int id = context.getResources().getIdentifier(this.getCharacter().toString(), "drawable",
                context.getPackageName());

        Drawable t = context.getResources().getDrawable(id);

        t.setBounds(x, y, x + width, y + height);
        t.draw(canvas);
    }

    public void drawTile3(Context context, Canvas canvas, int width, int height, int x, int y, int num) {
        // draw the cards
        int id = context.getResources().getIdentifier(this.getCharacter().toString(), "drawable",
                context.getPackageName());

        Drawable t = context.getResources().getDrawable(id);
        t.mutate();

        t.setAlpha(num);

        t.setBounds(x,y,x+width,y+height);
        t.draw(canvas);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tile)) return false;

        Tile tile = (Tile) o;

        if (alphaNum != tile.alphaNum) return false;
        if (isPartOfWord != tile.isPartOfWord) return false;
        if (value != tile.value) return false;
        if (!letter.equals(tile.letter)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = letter.hashCode();
        result = 31 * result + value;
        result = 31 * result + alphaNum;
        result = 31 * result + (isPartOfWord ? 1 : 0);
        return result;
    }
}